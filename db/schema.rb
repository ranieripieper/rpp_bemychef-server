# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151105143200) do

  create_table "addresses", force: :cascade do |t|
    t.string "street",       limit: 255, null: false
    t.string "number",       limit: 255
    t.string "complement",   limit: 255
    t.string "neighborhood", limit: 255, null: false
    t.string "zip_code",     limit: 9,   null: false
    t.string "city",         limit: 100, null: false
    t.string "state",        limit: 30,  null: false
    t.string "country",      limit: 30,  null: false
    t.float  "latitude",     limit: 24,  null: false
    t.float  "longitude",    limit: 24,  null: false
  end

  create_table "bank_account_infos", force: :cascade do |t|
    t.string   "name",            limit: 255, null: false
    t.string   "bank",            limit: 255, null: false
    t.string   "agency",          limit: 255, null: false
    t.string   "current_account", limit: 255, null: false
    t.string   "document",        limit: 255, null: false
    t.integer  "chef_id",         limit: 4,   null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "bank_account_infos", ["chef_id"], name: "index_bank_account_infos_on_chef_id", using: :btree

  create_table "cancel_payments", force: :cascade do |t|
    t.string   "message",        limit: 255, null: false
    t.string   "transaction_id", limit: 255, null: false
    t.string   "status",         limit: 255, null: false
    t.string   "gateway",        limit: 255, null: false
    t.integer  "user_id",        limit: 4
    t.integer  "ordered_id",     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cancel_payments", ["ordered_id"], name: "cancel_payments_ordered_id_fk", using: :btree
  add_index "cancel_payments", ["user_id"], name: "cancel_payments_user_id_fk", using: :btree

  create_table "chef_notifications", force: :cascade do |t|
    t.string   "text",              limit: 255, null: false, comment: "Texto para exibir para o usuário."
    t.integer  "notification_type", limit: 4,   null: false, comment: "Tipo da notificação. Pedido aceito, pedido recusado, pedido cancelado..."
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ordered_id",        limit: 4,   null: false
    t.integer  "chef_id",           limit: 4,   null: false
  end

  add_index "chef_notifications", ["chef_id"], name: "chef_notifications_chef_id_fk", using: :btree
  add_index "chef_notifications", ["ordered_id"], name: "chef_notifications_ordered_id_fk", using: :btree

  create_table "chefs", force: :cascade do |t|
    t.float    "avg_rating",            limit: 24,       default: 0.0, null: false
    t.float    "nr_rating",             limit: 24,       default: 0.0, null: false
    t.integer  "qt_cancel_ordered",     limit: 4,        default: 0,   null: false
    t.integer  "qt_not_accept_ordered", limit: 4,        default: 0,   null: false
    t.integer  "qt_timeout_ordered",    limit: 4,        default: 0,   null: false
    t.text     "about",                 limit: 16777215,               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id",               limit: 4,                      null: false
  end

  add_index "chefs", ["user_id"], name: "chefs_user_id_fk", using: :btree

  create_table "comments", force: :cascade do |t|
    t.string   "comment",    limit: 255,                 null: false
    t.integer  "rating",     limit: 4,                   null: false
    t.boolean  "approved",   limit: 1,   default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "dish_id",    limit: 4,                   null: false
    t.integer  "user_id",    limit: 4,                   null: false
    t.integer  "photo_id",   limit: 4
    t.integer  "ordered_id", limit: 4,                   null: false
  end

  add_index "comments", ["dish_id"], name: "comments_dish_id_fk", using: :btree
  add_index "comments", ["ordered_id"], name: "comments_ordered_fk", using: :btree
  add_index "comments", ["photo_id"], name: "comments_photo_id_fk", using: :btree
  add_index "comments", ["user_id"], name: "comments_user_id_fk", using: :btree

  create_table "dish_ingredients", force: :cascade do |t|
    t.integer "dish_id",       limit: 4, null: false
    t.integer "ingredient_id", limit: 4, null: false
  end

  add_index "dish_ingredients", ["dish_id"], name: "dish_ingredients_dish_id_fk", using: :btree
  add_index "dish_ingredients", ["ingredient_id"], name: "dish_ingredients_ingredient_id_fk", using: :btree

  create_table "dishes", force: :cascade do |t|
    t.string   "name",              limit: 255,                      null: false
    t.text     "description",       limit: 16777215,                 null: false
    t.integer  "dish_type",         limit: 4,                                     comment: "tipo de prato - (0) nenhum, (1) Vegetariano, Vegano, (2) Ovo-lacto-vegetarianos, (3) Ovo-vegetarianos, (4) Lacto-vegetarianos"
    t.boolean  "organic",           limit: 1,        default: false, null: false, comment: "se prato é orgânico"
    t.float    "price",             limit: 24,                       null: false, comment: "preço do prato"
    t.integer  "quantity",          limit: 4,                        null: false, comment: "quantidade para ser vendida"
    t.integer  "quantity_reserved", limit: 4,        default: 0,     null: false, comment: "quantidade reservada"
    t.integer  "quantity_sale",     limit: 4,        default: 0,     null: false, comment: "quantidade vendida"
    t.integer  "time_to_prepare",   limit: 4,                        null: false, comment: "tempo de preparo do prato em minutos"
    t.boolean  "available",         limit: 1,        default: false, null: false, comment: "se o prato está disponível para venda. "
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "kitchen_id",        limit: 4,                        null: false
    t.datetime "deleted_at"
  end

  add_index "dishes", ["deleted_at"], name: "index_dishes_on_deleted_at", using: :btree
  add_index "dishes", ["kitchen_id"], name: "dishes_kitchen_id_fk", using: :btree

  create_table "ingredients", force: :cascade do |t|
    t.string  "name",    limit: 255, null: false, comment: "nome do ingrediente em inglês"
    t.string  "name_pt", limit: 255, null: false, comment: "nome do ingrediente em português"
    t.boolean "animal",  limit: 1,   null: false
    t.boolean "lacto",   limit: 1,   null: false
    t.boolean "egg",     limit: 1,   null: false
  end

  create_table "kitchens", force: :cascade do |t|
    t.boolean  "close",            limit: 1,   default: false, null: false
    t.boolean  "verified",         limit: 1,   default: false, null: false
    t.boolean  "approved",         limit: 1,   default: false, null: false
    t.datetime "dt_blocked"
    t.datetime "dt_open"
    t.datetime "dt_close"
    t.integer  "qt_packing",       limit: 4,   default: 0
    t.integer  "qt_packing_userd", limit: 4,   default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "address_id",       limit: 4,                   null: false
    t.integer  "chef_id",          limit: 4,                   null: false
    t.boolean  "food_truck",       limit: 1,   default: false, null: false
    t.string   "phone",            limit: 255,                 null: false
    t.boolean  "delivery",         limit: 1,   default: false, null: false, comment: "se o chef faz a entrega"
    t.boolean  "pick_up",          limit: 1,   default: false, null: false, comment: "se pode retirar com o chef"
  end

  add_index "kitchens", ["address_id"], name: "kitchens_address_id_fk", using: :btree
  add_index "kitchens", ["chef_id"], name: "kitchens_chef_id_fk", using: :btree

  create_table "ordered_statuses", force: :cascade do |t|
    t.integer  "status_id",  limit: 4, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ordered_id", limit: 4, null: false
  end

  add_index "ordered_statuses", ["ordered_id"], name: "ordered_statuses_ordered_id_fk", using: :btree

  create_table "ordereds", force: :cascade do |t|
    t.boolean  "paid_for_chef",          limit: 1,  default: false
    t.datetime "dt_paid_for_chef"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "address_id",             limit: 4,                  null: false
    t.integer  "last_ordered_status_id", limit: 4,                  null: false
    t.integer  "quantity",               limit: 4,                  null: false, comment: "quantidade de pratos pedidos"
    t.integer  "delivery_pick_up",       limit: 4,  default: 1,     null: false, comment: "se o chef/motoboy faz a entrega ou se o usuário retira. Valores válidos: 1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy delivery"
    t.float    "ordered_value",          limit: 24,                 null: false, comment: "Valor do pedido."
    t.float    "delivery_price",         limit: 24,                 null: false, comment: "Valor do delivery."
    t.float    "dish_price",             limit: 24,                 null: false, comment: "Preço do prato."
    t.integer  "dish_id",                limit: 4,                  null: false
    t.integer  "user_id",                limit: 4,                  null: false
  end

  add_index "ordereds", ["address_id"], name: "ordereds_address_id_fk", using: :btree
  add_index "ordereds", ["dish_id"], name: "ordereds_dishes_fk", using: :btree
  add_index "ordereds", ["user_id"], name: "ordereds_users_fk", using: :btree

  create_table "payments", force: :cascade do |t|
    t.float    "value",          limit: 24,  null: false, comment: "Valor da transação."
    t.string   "transaction_id", limit: 255, null: false, comment: "Id da transação no gateway."
    t.string   "status",         limit: 255, null: false, comment: "Status do pagamento - Verificar documento do gateway correspondente."
    t.string   "card_flag",      limit: 255, null: false, comment: "Bandeira do cartão: visa, master..."
    t.string   "last_card_nr",   limit: 255, null: false, comment: "Últimos 4 digitos do cartão."
    t.string   "gateway",        limit: 255, null: false, comment: "Cielo, pagar.me, rede..."
    t.boolean  "can_capture",    limit: 1,                comment: "Se true, pode realizar a captura da transação. Se false, a transação será cancelada"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ordered_id",     limit: 4,   null: false
  end

  add_index "payments", ["ordered_id"], name: "payments_ordered_id_fk", using: :btree

  create_table "photos", force: :cascade do |t|
    t.integer  "kitchen_id",        limit: 4
    t.integer  "dish_id",           limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
  end

  add_index "photos", ["dish_id"], name: "photos_dish_id_fk", using: :btree
  add_index "photos", ["kitchen_id"], name: "photos_kitchen_id_fk", using: :btree

  create_table "reserves", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quantity",         limit: 4,                  null: false, comment: "quantidade de pratos pedidos"
    t.integer  "delivery_pick_up", limit: 4,  default: 1,     null: false, comment: "se o chef/motoboy faz a entrega ou se o usuário retira. Valores válidos: 1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy delivery"
    t.float    "ordered_value",    limit: 24,                 null: false, comment: "Valor do pedido."
    t.float    "delivery_price",   limit: 24,                 null: false, comment: "Valor do delivery."
    t.integer  "dish_id",          limit: 4,                  null: false
    t.integer  "user_id",          limit: 4,                  null: false
    t.integer  "attempts",         limit: 2,  default: 0,     null: false, comment: "Quantidade de vezes que o usuário tentou confirmar o pedido"
    t.boolean  "canceled",         limit: 1,  default: false, null: false, comment: "Se a reserva é válida ou não. true - reserva válida | false - reserva inválida"
    t.boolean  "confirmed",        limit: 1,  default: false, null: false, comment: "Se a reserva foi confirmada (Virou um pedido)"
  end

  add_index "reserves", ["dish_id"], name: "fk_rails_e9331e8a95", using: :btree
  add_index "reserves", ["user_id"], name: "fk_rails_2a3188508f", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255,   null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "subscriptions", force: :cascade do |t|
    t.string   "email",      limit: 255, default: "",    null: false
    t.string   "name",       limit: 255,                 null: false
    t.string   "zip_code",   limit: 255,                 null: false
    t.string   "phone",      limit: 255
    t.string   "country",    limit: 255,                 null: false
    t.boolean  "chef",       limit: 1,                   null: false
    t.boolean  "email_sent", limit: 1,   default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_notifications", force: :cascade do |t|
    t.string   "text",              limit: 255, null: false, comment: "Texto para exibir para o usuário."
    t.integer  "notification_type", limit: 4,   null: false, comment: "Tipo da notificação. Pedido aceito, pedido recusado, pedido cancelado..."
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ordered_id",        limit: 4,   null: false
    t.integer  "comment_id",        limit: 4
    t.integer  "user_id",           limit: 4,   null: false
  end

  add_index "user_notifications", ["comment_id"], name: "user_notifications_comment_id_fk", using: :btree
  add_index "user_notifications", ["ordered_id"], name: "user_notifications_ordered_id_fk", using: :btree
  add_index "user_notifications", ["user_id"], name: "user_notifications_user_id_fk", using: :btree

  create_table "user_push_tokens", force: :cascade do |t|
    t.string   "token",      limit: 255,                null: false, comment: "Token ios/android"
    t.string   "platform",   limit: 255,                null: false
    t.boolean  "notify",     limit: 1,   default: true, null: false, comment: "Se usuário aceita receber notificação neste token"
    t.integer  "user_id",    limit: 4,                  null: false, comment: "Usuário"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "user_push_tokens", ["user_id"], name: "fk_rails_b4cec2c4e8", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",      null: false
    t.string   "encrypted_password",     limit: 255, default: "",      null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   limit: 255,                   null: false
    t.string   "facebook_token",         limit: 255
    t.string   "facebook_id",            limit: 255
    t.string   "auth_token",             limit: 255
    t.string   "locale",                 limit: 10,  default: "pt-BR", null: false
    t.integer  "photo_id",               limit: 4
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["photo_id"], name: "users_photo_id_fk", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "bank_account_infos", "chefs"
  add_foreign_key "cancel_payments", "ordereds", name: "cancel_payments_ordered_id_fk", on_delete: :cascade
  add_foreign_key "cancel_payments", "users", name: "cancel_payments_user_id_fk", on_delete: :cascade
  add_foreign_key "chef_notifications", "chefs", name: "chef_notifications_chef_id_fk", on_delete: :cascade
  add_foreign_key "chef_notifications", "ordereds", name: "chef_notifications_ordered_id_fk", on_delete: :cascade
  add_foreign_key "chefs", "users", name: "chefs_user_id_fk", on_delete: :cascade
  add_foreign_key "comments", "dishes", name: "comments_dish_id_fk", on_delete: :cascade
  add_foreign_key "comments", "ordereds", name: "comments_ordered_fk"
  add_foreign_key "comments", "photos", name: "comments_photo_id_fk", on_delete: :cascade
  add_foreign_key "comments", "users", name: "comments_user_id_fk", on_delete: :cascade
  add_foreign_key "dishes", "kitchens", name: "dishes_kitchen_id_fk", on_delete: :cascade
  add_foreign_key "kitchens", "addresses", name: "kitchens_address_id_fk", on_delete: :cascade
  add_foreign_key "kitchens", "chefs", name: "kitchens_chef_id_fk", on_delete: :cascade
  add_foreign_key "ordered_statuses", "ordereds", name: "ordered_statuses_ordered_id_fk", on_delete: :cascade
  add_foreign_key "ordereds", "addresses", name: "ordereds_address_id_fk", on_delete: :cascade
  add_foreign_key "ordereds", "dishes", name: "ordereds_dishes_fk"
  add_foreign_key "ordereds", "users", name: "ordereds_users_fk"
  add_foreign_key "payments", "ordereds", name: "payments_ordered_id_fk", on_delete: :cascade
  add_foreign_key "photos", "dishes", name: "photos_dish_id_fk", on_delete: :cascade
  add_foreign_key "photos", "kitchens", name: "photos_kitchen_id_fk", on_delete: :cascade
  add_foreign_key "reserves", "dishes"
  add_foreign_key "reserves", "users"
  add_foreign_key "user_notifications", "comments", name: "user_notifications_comment_id_fk", on_delete: :cascade
  add_foreign_key "user_notifications", "ordereds", name: "user_notifications_ordered_id_fk", on_delete: :cascade
  add_foreign_key "user_notifications", "users", name: "user_notifications_user_id_fk", on_delete: :cascade
  add_foreign_key "user_push_tokens", "users"
  add_foreign_key "users", "photos", name: "users_photo_id_fk", on_delete: :cascade
end
