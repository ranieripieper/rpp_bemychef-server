class CreateChefNotifications < ActiveRecord::Migration
  def change
    create_table :chef_notifications do |t|
      t.string      :text,                    null: false, :comment => 'Texto para exibir para o usuário.'
      t.integer     :notification_type,       null: false, :comment => 'Tipo da notificação. Pedido aceito, pedido recusado, pedido cancelado...'
      t.timestamps
      
      t.integer     :ordered_id, null: false
      
      t.integer     :chef_id, null: false
    end
    
    add_foreign_key :chef_notifications, :ordereds
    add_foreign_key :chef_notifications, :chefs
  end
end
