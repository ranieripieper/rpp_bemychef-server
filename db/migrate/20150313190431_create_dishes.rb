class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string      :name,                   null: false
      t.string      :description,            null: false
      t.integer     :dish_type,              null: true, :comment => 'tipo de prato - (0) nenhum, (1) Vegetariano, Vegano, (2) Ovo-lacto-vegetarianos, (3) Ovo-vegetarianos, (4) Lacto-vegetarianos'
      t.boolean     :organic,                null: false, default: false, :comment => 'se prato é orgânico'
      t.boolean     :delivery,               null: false, default: false, :comment => 'se o chef faz a entrega'
      t.boolean     :pick_up,                null: false, default: false, :comment => 'se pode retirar com o chef'
      
      t.float       :price,                  null: false, :comment => 'preço do prato'
      t.integer     :quantity,               null: false, :comment => 'quantidade para ser vendida'
      t.integer     :quantity_reserved,      null: false, default: 0, :comment => 'quantidade reservada'
      t.integer     :quantity_sale,          null: false, default: 0, :comment => 'quantidade vendida'
      t.integer     :time_to_prepare,        null: false, :comment => 'tempo de preparo do prato em minutos'
      t.boolean     :available,              null: false, default: false, :comment => 'se o prato está disponível para venda. '

      t.timestamps
      
      t.integer     :kitchen_id, null: false
    end
    
    add_foreign_key :dishes, :kitchens
  end
end
