class AdjustReserveModel < ActiveRecord::Migration
  def change
    change_table(:reserves) do |t|
      t.integer     :quantity,                  null: false, :comment => 'quantidade de pratos pedidos'
      t.integer     :delivery_pick_up,          null: false, default: 1, :comment => 'se o chef/motoboy faz a entrega ou se o usuário retira. Valores válidos: 1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy delivery'
      
      t.float       :ordered_value,             null: false, :comment => 'Valor do pedido.'
      t.float       :delivery_price,            null: false, :comment => 'Valor do delivery.'
      
      t.boolean     :reserve_valid,             null: false, :comment => 'Se a reserva é válida ou não. true - reserva válida | false - reserva inválida'
      
      t.integer     :dish_id, null: false
      
      t.integer     :user_id, null: false
    end
    
    add_foreign_key :reserves, :dishes
    add_foreign_key :reserves, :users
  end
end
