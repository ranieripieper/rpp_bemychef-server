class AdjustOrderedReserveModel < ActiveRecord::Migration
  def change
   
    change_table(:reserves) do |t|
      t.remove      :reserve_valid
      
      t.integer     :attempts,    null: false, default: 0, limit: 2, :comment => 'Quantidade de vezes que o usuário tentou confirmar o pedido'
      t.boolean     :canceled,    null: false, default: false, :comment => 'Se a reserva é válida ou não. true - reserva válida | false - reserva inválida'
      t.boolean     :confirmed,   null: false, default: false, :comment => 'Se a reserva foi confirmada (Virou um pedido)'

    end
    
    remove_foreign_key :ordereds, :dishes
    remove_foreign_key :ordereds, :users
    
    change_table(:ordereds) do |t|
      t.remove      :quantity
      t.remove      :delivery_pick_up
      t.remove      :ps
      t.remove      :price
      t.remove      :dish_price
      t.remove      :delivery_price
      t.remove      :dish_id
      t.remove      :user_id
      
      t.integer     :reserve_id, null: false
    end
 
    add_foreign_key :ordereds, :reserves, column: :reserve_id

  end
  
end
