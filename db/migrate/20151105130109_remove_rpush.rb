class RemoveRpush < ActiveRecord::Migration
  def change
      drop_table :rpush_notifications
      drop_table :rpush_feedback      
      drop_table :rpush_apps
  end
end
