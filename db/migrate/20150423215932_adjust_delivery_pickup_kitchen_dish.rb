class AdjustDeliveryPickupKitchenDish < ActiveRecord::Migration
  def change
    
    change_table(:kitchens) do |t|
      t.boolean     :delivery,               null: false, default: false, :comment => 'se o chef faz a entrega'
      t.boolean     :pick_up,                null: false, default: false, :comment => 'se pode retirar com o chef'
    end

    change_table(:dishes) do |t|
      t.remove     :delivery
      t.remove     :pick_up
    end
    
  end
end
