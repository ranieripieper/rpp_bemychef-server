class CreateUserPushTokens < ActiveRecord::Migration
  def change
    create_table :user_push_tokens do |t|

      t.string    :token,       null: false, :comment => "Token ios/android"
      t.string    :plataform,   null: false, :comment => "Plataforma - ios/android"
      t.boolean   :notify,      null: false, default: true, :comment => "Se usuário aceita receber notificação neste token"
      t.integer   :user_id,     null: false, :comment => "Usuário"
      
      t.timestamps null: false
    end
    
    add_foreign_key :user_push_tokens, :users
    
    change_table(:users) do |t|
      t.remove     :device_id
    end
  end
end
