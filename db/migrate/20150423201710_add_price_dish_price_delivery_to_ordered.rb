class AddPriceDishPriceDeliveryToOrdered < ActiveRecord::Migration
  def change
    
    change_table(:ordereds) do |t|
      t.float :dish_price, null: false
      t.float :delivery_price, null: false
    end
    
  end
end
