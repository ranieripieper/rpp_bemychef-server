class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|

      #endereço
      t.string :street,                   null: false
      t.string :number
      t.string :complement            
      t.string :neighborhood,             null: false
      t.string :zip_code,                 null: false, limit:9
      t.string :city,                     null: false, limit:100
      t.string :state,                    null: false, limit:30
      t.string :country,                  null: false, limit:30
      t.float :latitude,                  null: false
      t.float :longitude,                 null: false

    end
  end
end
