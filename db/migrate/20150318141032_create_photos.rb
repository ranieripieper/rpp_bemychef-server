class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|

      t.integer       :kitchen_id, null: true
      
      t.integer       :dish_id, null: true
      
      t.timestamps
    end
    
    add_foreign_key :photos, :kitchens
    add_foreign_key :photos, :dishes
    
    #fotos
    add_attachment :photos, :file
    
    #adiciona photos_id na tabela de usuário
    change_table(:users) do |t|
      t.integer       :photo_id, null: true
    end
    
    add_foreign_key :users, :photos
    
  end
end
