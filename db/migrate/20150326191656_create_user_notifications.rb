class CreateUserNotifications < ActiveRecord::Migration
  def change
    create_table :user_notifications do |t|
      t.string      :text,                    null: false, :comment => 'Texto para exibir para o usuário.'
      t.integer     :notification_type,       null: false, :comment => 'Tipo da notificação. Pedido aceito, pedido recusado, pedido cancelado...'
      t.timestamps
      
      t.integer     :ordered_id, null: false
      
      t.integer     :comment_id, null: true
      
      t.integer     :user_id, null: false
    end
    
    add_foreign_key :user_notifications, :ordereds
    add_foreign_key :user_notifications, :comments
    add_foreign_key :user_notifications, :users
    
  end
end
