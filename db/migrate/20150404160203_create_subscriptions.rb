class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|

      t.string :email,              null: false, default: ""
      t.string :name,               null: false
      t.string :zip_code,           null: false
      t.string :phone,              null: true
      t.string :country,            null: false
      t.boolean :chef,              null: false
      t.boolean :email_sent,        null: true, default: false

      t.timestamps
    end
  end
end
