class AddFoodTruckPhoneToKitchens < ActiveRecord::Migration
  def change
    change_table(:kitchens) do |t|
      t.boolean     :food_truck,          null: false, default: false
      t.string      :phone,               null: false
    end
  end
end
