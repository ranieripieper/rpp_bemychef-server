class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|

      t.string      :name,                    null: false, :comment => 'nome do ingrediente em inglês'
      t.string      :name_pt,                 null: false, :comment => 'nome do ingrediente em português'
      t.boolean     :animal,                  null: false
      t.boolean     :lacto,                   null: false
      t.boolean     :egg,                     null: false

    end
  end
end
