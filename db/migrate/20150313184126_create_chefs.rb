class CreateChefs < ActiveRecord::Migration
  def change
    create_table :chefs do |t|
      t.float     :avg_rating,            null: false, default: 0.0
      t.float     :nr_rating,             null: false, default: 0
      t.integer   :qt_cancel_ordered,     null: false, default: 0
      t.integer   :qt_not_accept_ordered, null: false, default: 0
      t.integer   :qt_timeout_ordered,    null: false, default: 0
      t.string    :about,                 null: false
      
      t.timestamps
      
      
      t.integer     :user_id, null: false, index: true
    end
    
    add_foreign_key :chefs, :users
  end
end
