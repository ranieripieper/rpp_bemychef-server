class AdjustOrderedModel < ActiveRecord::Migration
  def change

    remove_foreign_key :ordereds, column: :reserve_id
    
    change_table(:ordereds) do |t|
      t.integer     :quantity,                  null: false, :comment => 'quantidade de pratos pedidos'
      t.integer     :delivery_pick_up,          null: false, default: 1, :comment => 'se o chef/motoboy faz a entrega ou se o usuário retira. Valores válidos: 1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy delivery'
      
      t.float       :ordered_value,             null: false, :comment => 'Valor do pedido.'
      t.float       :delivery_price,            null: false, :comment => 'Valor do delivery.'
      t.float       :dish_price,                null: false, :comment => 'Preço do prato.'

      t.integer     :dish_id, null: false     
      t.integer     :user_id, null: false
      
      t.remove     :reserve_id
    end
    add_foreign_key :ordereds, :dishes, :name => 'ordereds_dishes_fk'
    add_foreign_key :ordereds, :users, :name => 'ordereds_users_fk'
    
  end
end
