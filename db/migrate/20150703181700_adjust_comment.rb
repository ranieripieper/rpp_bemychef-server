class AdjustComment < ActiveRecord::Migration
  def change
    
    change_table(:comments) do |t|
      t.integer     :ordered_id,           null: false
    end
    
    add_foreign_key :comments, :ordereds, :name => 'comments_ordered_fk'
    
  end

end
