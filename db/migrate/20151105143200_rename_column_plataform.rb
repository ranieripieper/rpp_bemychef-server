class RenameColumnPlataform < ActiveRecord::Migration
  
  def up
    rename_column :user_push_tokens, :plataform, :platform
  end
 
  def down
    rename_column :user_push_tokens, :platform, :plataform
  end
end
