class CreateOrdereds < ActiveRecord::Migration
  def change
    create_table :ordereds do |t|

      t.integer     :quantity,                  null: false, :comment => 'quantidade de pratos pedidos'
      t.integer     :delivery_pick_up,          null: false, default: 1, :comment => 'se o chef/motoboy faz a entrega ou se o usuário retira. Valores válidos: 1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy delivery'
      
      t.string      :ps,                        null: true, :comment => 'Observação do pedido'
      
      t.boolean     :paid_for_chef,             null: true, default: false
      t.timestamp   :dt_paid_for_chef,          null: true
      t.float       :price,                     null: false, :comment => 'Valor do pedido.'
      
      t.timestamps
      
      t.integer     :dish_id, null: false
      
      t.integer     :user_id, null: false
      
      t.integer     :address_id, null: false
      
      t.integer     :last_ordered_status_id, null: false
      
    end
    
    add_foreign_key :ordereds, :dishes
    add_foreign_key :ordereds, :users
    add_foreign_key :ordereds, :addresses
    
  end
end
