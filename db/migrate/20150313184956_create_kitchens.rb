class CreateKitchens < ActiveRecord::Migration
  def change
    create_table :kitchens do |t|
      t.boolean     :close,               null: false, default: true
      t.boolean     :verified,            null: false, default: false
      t.boolean     :approved,            null: false, default: false
      t.timestamp   :dt_blocked,          null: true
      t.timestamp   :dt_open,             null: true
      t.timestamp   :dt_close,            null: true
      t.integer     :qt_packing,          null: true, default: 0
      t.integer     :qt_packing_userd,    null: true, default: 0
      t.timestamps
      
      t.integer     :address_id, null: false
      t.foreign_key :addresses, dependent: :delete
      
      t.integer     :chef_id, null: false      
    end
    
    add_foreign_key :kitchens, :chefs
    
  end
end
