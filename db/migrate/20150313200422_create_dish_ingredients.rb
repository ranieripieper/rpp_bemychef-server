class CreateDishIngredients < ActiveRecord::Migration
  def change
    create_table :dish_ingredients do |t|
      
      t.integer     :dish_id, null: false
      t.foreign_key :dishes, dependent: :delete
      
      t.integer     :ingredient_id, null: false
      
    end
    
    add_foreign_key :dish_ingredients, :ingredients
  end
end
