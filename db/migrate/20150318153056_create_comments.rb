class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string      :comment,                   null: false
      t.integer     :rating,                    null: false
      t.boolean     :approved,                  null: false, default: false
      t.timestamps
      
      t.integer     :dish_id, null: false
      
      t.integer     :user_id, null: false
      
      t.integer     :photo_id, null: true
    end
    
    add_foreign_key :comments, :dishes
    add_foreign_key :comments, :users
    add_foreign_key :comments, :photos

  end
end
