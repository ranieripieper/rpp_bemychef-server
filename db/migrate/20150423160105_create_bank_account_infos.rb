class CreateBankAccountInfos < ActiveRecord::Migration
  def change
    create_table :bank_account_infos do |t|
      t.string    :name, null: false
      t.string    :bank, null: false
      t.string    :agency, null: false
      t.string    :current_account , null: false
      t.string    :document , null: false
      
      t.integer     :chef_id, null: false, index: true
      
      t.timestamps null: false
    end
    
    change_table(:users) do |t|
      t.remove :document
    end
    
    add_foreign_key :bank_account_infos, :chefs
  end
end
