class CreateOrderedStatuses < ActiveRecord::Migration
  def change
    create_table :ordered_statuses do |t|

      t.integer     :status_id, null: false
      t.timestamps
      
      t.integer     :ordered_id, null: false
      
    end
    
    add_foreign_key :ordered_statuses, :ordereds
  end
end
