class CreatePayments < ActiveRecord::Migration

  def change
    
    create_table(:payments) do |t|
      t.float   :value,              null: false, :comment => 'Valor da transação.'
      t.string  :transaction_id,     null: false, :comment => 'Id da transação no gateway.'
      t.string  :status,             null: false, :comment => 'Status do pagamento - Verificar documento do gateway correspondente.'
      t.string  :card_flag,          null: false, :comment => 'Bandeira do cartão: visa, master...'
      t.string  :last_card_nr,       null: false, :comment => 'Últimos 4 digitos do cartão.'
      t.string  :gateway,            null: false, :comment => 'Cielo, pagar.me, rede...'
      t.boolean :can_capture,        null: true,  :comment => 'Se true, pode realizar a captura da transação. Se false, a transação será cancelada'
      t.timestamps
      
      t.integer       :ordered_id, null: false
    end
    
    add_foreign_key :payments, :ordereds
    
  end
end
