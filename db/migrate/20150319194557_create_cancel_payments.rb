class CreateCancelPayments < ActiveRecord::Migration
  def change
    
    create_table(:cancel_payments) do |t|
      t.string      :message,            null: false
      t.string      :transaction_id,     null: false
      t.string      :status,             null: false
      t.string      :gateway,            null: false
      
      t.integer     :user_id,              null: true
      
      t.integer     :ordered_id,           null: true
      
      t.timestamps
    end
    
    add_foreign_key :cancel_payments, :users
    add_foreign_key :cancel_payments, :ordereds
    
  end
end
