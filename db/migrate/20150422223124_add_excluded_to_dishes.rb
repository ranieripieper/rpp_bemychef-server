class AddExcludedToDishes < ActiveRecord::Migration
  def change
    change_table(:dishes) do |t|
      t.boolean     :excluded,          null: false, default: false
    end
  end
end
