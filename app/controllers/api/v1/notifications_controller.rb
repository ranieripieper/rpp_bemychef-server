class Api::V1::NotificationsController < Api::V1::ApiController
  
  before_action :authenticate_user!, only: [:get_chef_notifications, :get_user_notifications, :get_chef_notification_by_id, :get_user_notification_by_id]

  resource_description do
    formats ['json']
  end

  api :GET, "/api/v1/chef/notifications.json", "Retorna as notificações que o chef recebeu [Chef]"
  param :page, :number, :required => true, :desc => "Número da página - Deve ser maior ou igual a 1."
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/notifications/chef.json?page=1"
  def get_chef_notifications
    user = get_user()
    page = params[:page].to_i
    if page.nil? or page == 0 then
      page = 1
    end
    if user.chef.nil? then
      render :status => 200,
                 :json => { :success => true, :notifications => nil}
    else
      notifications = ChefNotification.where(:chef_id => user.chef.id).paginate(:page => page)
      render :status => 200,
                 :json => { :success => true, :notifications => notifications}
    end   
  end

  api :GET, "/api/v1/notifications/user.json", "Retorna as notificações que o usuário recebeu [User]"
  param :page, :number, :required => true, :desc => "Número da página - Deve ser maior ou igual a 1."
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/notifications/user.json?page=1"
  def get_user_notifications
    user = get_user()
    page = params[:page].to_i
    if page.nil? or page == 0 then
      page = 1
    end

    notifications = UserNotification.where(:user_id => user.id).paginate(:page => page)
    render :status => 200,
               :json => { :success => true, :notifications => notifications}
 
  end

  api :GET, "/api/v1/notifications/chef/:notification_id.json", "Retorna a notificação pelo ID [Chef]"
  param :notification_id, :number, :required => true, :desc => "Id da notificação"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/notifications/chef/1.json"
  def get_chef_notification_by_id
    user = get_user()
    notification_id = params[:notification_id].to_i
    
    if user.chef.nil? then
      render :status => 200,
                 :json => { :success => true, :notification => nil}
    else
      notification = ChefNotification.joins(:ordered => :dish)
                        .includes(:ordered => :dish)
                        .where(:chef_id => user.chef.id, :id => notification_id).first
        render :status => 200,
               :json => { :success => true, :notification => notification.as_json(:ordered => true)}
    end   
    
    
 
  end
  
  api :GET, "/api/v1/notifications/user/:notification_id.json", "Retorna a notificação pelo ID [User]"
  param :notification_id, :number, :required => true, :desc => "Id da notificação"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/notifications/user/1.json"
  def get_user_notification_by_id
    user = get_user()
    notification_id = params[:notification_id].to_i
    

    notification = UserNotification.joins(:ordered => :dish)
                        .joins('LEFT OUTER JOIN `comments` ON `comments`.`id` = `user_notifications`.`comment_id` ')
                        .includes(:ordered => :dish)
                        .includes(:comment)
                        .where(:user_id => user.id, :id => notification_id).first
    render :status => 200,
               :json => { :success => true, :notification => notification.as_json(:ordered => true)}
 
  end
  
  

end