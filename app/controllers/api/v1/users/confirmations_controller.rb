class Api::V1::Users::ConfirmationsController < Devise::ConfirmationsController

  skip_before_filter :verify_authenticity_token
                     #:if => Proc.new { |c| c.request.format == 'application/json' }

  resource_description do
    api_version "v1"
    formats ['json']
  end
  
  api :POST, "/api/v1/users/reconfirm.json", "Reenvia email para o usuário com as instruções para ativação da conta [User/Chef]"
  param :email, String, :required => true, :desc => "Email do usuário"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example 'curl -F "email=ranieripieper@gmail.com" http://bmc.dev.com:3000/api/v1/users/reconfirm.json'
  def reconfirm
    email = params[:email]
    if email.nil? then
      render :status => 200,
         :json => { :success => false,
                      :info => I18n.t(:msg_invalid_parameters)}
    else
      user = User.find_by_email(email)
      if user.nil? then
        render :status => 200,
          :json => { :success => false,
                      :info => I18n.t(:msg_entity_not_found)}
      else 
        #verficia se usuário já confirmou
        if user.confirmed? then
          render :status => 200,
            :json => { :success => false,
                        :info => I18n.t(:msg_user_alreay_confirmed)}
        else
          user.send_confirmation_instructions
          render :status => 200,
                 :json => { :success => true, 
                            :info => I18n.t(:msg_resend_confirmation_instructions, user_name: user.name, user_email: user.email)}
        end
      end
    end  
  end
  
  def confirmed
    
  end
  
  def show
   self.resource = resource_class.confirm_by_token(params[:confirmation_token])

    if resource.errors.empty?
      set_flash_message(:notice, :confirmed) if is_navigational_format?
      sign_in(resource_name, resource)
      respond_with_navigational(resource){ redirect_to user_confirmed_path(resource.id) }
    else
      respond_with_navigational(resource.errors, :status => :unprocessable_entity){ render :new }
    end
  end
  
protected
  
  def set_csrf_header
     response.headers['X-CSRF-Token'] = form_authenticity_token
  end 
end