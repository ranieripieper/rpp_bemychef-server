# file: app/controller/api/v1/sessions_controller.rb
class Api::V1::Users::SessionsController < Devise::SessionsController

  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  respond_to :json
  
  resource_description do
    api_version "v1"
    formats ['json']
  end
  
  api :POST, "/api/v1/users/login.json", "Realiza o Login [User/Chef]"
  param :user, Hash, :required => true, :action_aware => true do
    param :email, String, :required => true, :desc => "Email"
    param :password, String, :required => true, :desc => "Senha"
  end
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example 'curl -F "user[email]=ranieripieper@gmail.com" -F "user[password]=secret123" http://bmc.dev.com:3000/api/v1/users/login.json '
  def create
    resource = User.find_for_database_authentication(:email => params[:user][:email])
    return invalid_login_attempt unless resource
  
    if resource.valid_password?(params[:user][:password])
      resource.ensure_authentication_token  #make sure the user has a token generated
      if resource.confirmed? then
        sign_in resource
        render :status => 200,
             :json => { :success => true,
                        :info => I18n.t(:msg_login_ok),
                        :user => resource.as_json(:chef_kitchens => true) }
        return
      else
        #usuário necessita confirmar cadastro
        render :status => 200,
             :json => { :success => false,
                        :info => I18n.t(:msg_need_confirmation),
                        :user => resource.as_json(:chef_kitchens => true, :exclude_token => true) }
      end

    else
      invalid_login_attempt
    end
  end

  #curl -v -H 'Content-Type: application/json' -H 'Accept: application/json' -X DELETE http://localhost:3000/api/v1/sessions/\?auth_token\=GPtR4Q5eVNf6-auLqGsV
  def logout
    warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    current_user.update_column(:authentication_token, nil)
    render :status => 200,
           :json => { :success => true,
                      :info => I18n.t(:msg_logout_success),
                      :data => {} }
  end

private
  def invalid_login_attempt
      warden.custom_failure!
      render :json => { :success => false, :info => I18n.t("devise.failure.invalid", :authentication_keys => "email")},  :success => false, :status => :unauthorized
  end
end