# file: app/controllers/api/v1/registrations_controller.rb
class Api::V1::Users::RegistrationsController < Devise::RegistrationsController
  
  include UserHelper

  prepend_before_filter :authenticate_user_from_token!

  skip_before_filter :verify_authenticity_token
                     #:if => Proc.new { |c| c.request.format == 'application/json' }

  after_filter :set_csrf_header, only: [:update, :create]
  
  #before_action :authenticate_user!, only: [:update]
  
  #prepend_before_filter :authenticate_scope!, only: [:update]

  respond_to :json
  
  resource_description do
    api_version "v1"
    formats ['json']
  end

  def_param_group :address do
    param :address_attributes, Hash, :required => true, :action_aware => true do
      param :street, String, :required => false, :desc => "Rua"
      param :number, String, :required => true, :desc => "Número"
      param :complement, String, :required => false, :desc => "Complemento"
      param :zip_code, String, :required => true, :desc => "CEP"
      param :country, String, :required => false, :desc => "País"
    end
  end
  
  def_param_group :account_info do
    param :bank_account_info_attributes, Hash, :required => true, :action_aware => true do
      param :name, String, :required => true, :desc => "Nome"
      param :bank, String, :required => true, :desc => "Banco"
      param :agency, String, :required => false, :desc => "Agência "
      param :current_account, String, :required => true, :desc => "Conta corrrento"
      param :document, String, :required => true, :desc => "Documento (CPF)"
    end
  end
  
  def_param_group :user_push_token_attr do
    param :user_push_tokens_attributes, Array, :required => false, :action_aware => true do
      param :token, String, :required => true, :desc => "Token"
      param :platform, ["ios", "android"], :required => true, :desc => "Plataforma - Valores válidos: ios e android"
    end
  end
  
  def_param_group :user do
    param :user, Hash, :required => true, :action_aware => true do
      param :email, String, :required => true, :desc => "Email"
      param :name, String, :required => true, :desc => "Nome"
      param :locale, String, :required => false, :desc => "Língua a ser usada nas notificações. Valor default: en |  Ex.: en, pt-BR..."
      param :password, String, :required => false, :desc => "Senha"
      param :password_confirmation, String, :required => false, :desc => "Confirmação da senha"
      param_group :photo, :desc => "Foto"
      param_group :chef, :desc => "Chef"
      param_group :user_push_token_attr, :desc => "Tokens de notificação"
    end
  end

  def_param_group :photo do
    param :photo_attributes, Hash, :required => false, :action_aware => true do
      param :file, ActionDispatch::Http::UploadedFile, :required => false, :desc => "Arquivo"
    end
  end
  
  def_param_group :chef do
    param :chef_attributes, Hash, :required => false, :action_aware => true do
      param :about, String, :required => true, :desc => "Sobre o chef"
      param_group :account_info, :desc => "Dados bancários"
      param_group :kitchen, :desc => "Cozinha"
    end
  end
  
  def_param_group :kitchen do
    param :kitchen_attributes, Hash, :required => false, :action_aware => true do
      param :food_truck, [true, false], :required => false, :desc => "Se a cozinha é um food truck ou não. Default false"
      param :phone, String, :required => true, :desc => "Telefone da cozinha"
      param_group :address, :desc => "Endereço"
      param_group :photo, :desc => "Fotos"
    end
  end
  
  api :POST, "/api/v1/users.json", "Cria um novo usuário [User/Chef]"
  description "Cria um novo usuário"
  param_group :user
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."  
  example "curl -F 'user[user_push_tokens_attributes][][token]=xxx' -F 'user[user_push_tokens_attributes][][platform]=ios' -F 'user[chef_attributes][bank_account_info_attributes][name]=Nome conta banco' -F 'user[chef_attributes][bank_account_info_attributes][bank]=341' -F 'user[chef_attributes][bank_account_info_attributes][agency]=3412' -F 'user[chef_attributes][bank_account_info_attributes][current_account]=09831-1' -F 'user[chef_attributes][bank_account_info_attributes][document]=03988876590' -F 'user[chef_attributes][about]=teste chef about' -F 'user[chef_attributes][kitchens_attributes][][phone]=1100000000' -F 'user[chef_attributes][kitchens_attributes][][food_truck]=false' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file_content_type]=image/png' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file]=@bmc_logo_share.png' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file_content_type]=image/png' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file]=@dds_logo.png' -F 'user[chef_attributes][kitchens_attributes][][address_attributes][street]=Rua Tuim'  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][number]=356' -F 'user[chef_attributes][kitchens_attributes][][address_attributes][complement]=Sala 1' -F 'user[chef_attributes][kitchens_attributes][][address_attributes][zip_code]=04514-100' -F 'user[photo_attributes][file_content_type]=image/png' -F 'user[photo_attributes][file]=@logo_BeMyChef.png' -F 'user[name]=nome usuário'  -F 'user[email]=user40@example.com'  -F 'user[password]=secret123'  -F 'user[password_confirmation]=secret123' -F 'user[locale]=en' #{MainYetting.HOST}/api/v1/users.json"
  example "curl -F 'user[chef_attributes][bank_account_info_attributes][name]=Nome conta banco' -F 'user[chef_attributes][bank_account_info_attributes][bank]=341' -F 'user[chef_attributes][bank_account_info_attributes][agency]=3412' -F 'user[chef_attributes][bank_account_info_attributes][current_account]=09831-1' -F 'user[chef_attributes][bank_account_info_attributes][document]=03988876590' -F 'user[chef_attributes][about]=teste chef about' -F 'user[chef_attributes][kitchens_attributes][][phone]=1100000000' -F 'user[chef_attributes][kitchens_attributes][][food_truck]=false' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file_content_type]=image/png' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file]=@bmc_logo_share.png' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file_content_type]=image/png' -F 'user[chef_attributes][kitchens_attributes][][photos_attributes][][file]=@dds_logo.png' -F 'user[chef_attributes][kitchens_attributes][][address_attributes][street]=Rua Tuim'  -F 'user[chef_attributes][kitchens_attributes][][address_attributes][number]=356' -F 'user[chef_attributes][kitchens_attributes][][address_attributes][complement]=Sala 1' -F 'user[chef_attributes][kitchens_attributes][][address_attributes][zip_code]=04514-100' -F 'user[photo_attributes][file_content_type]=image/png' -F 'user[photo_attributes][file]=@logo_BeMyChef.png' -F 'user[name]=nome usuário'  -F 'user[email]=user40@example.com'  -F 'user[password]=secret123'  -F 'user[password_confirmation]=secret123' -F 'user[locale]=en' #{MainYetting.HOST}/api/v1/users.json"
  example "curl -F 'user[photo_attributes][file_content_type]=image/png' -F 'user[photo_attributes][file]=@profile.png' -F 'user[name]=nome usuário'  -F 'user[email]=user38@example.com'  -F 'user[password]=secret123'  -F 'user[password_confirmation]=secret123' -F 'user[locale]=en' #{MainYetting.HOST}/api/v1/users.json"
  example "curl -F 'user[user_push_tokens_attributes][][token]=xxx' -F 'user[user_push_tokens_attributes][][platform]=android' -F 'user[photo_attributes][file_content_type]=image/png' -F 'user[photo_attributes][file]=@profile.png' -F 'user[name]=nome usuário'  -F 'user[email]=user38@example.com'  -F 'user[password]=secret123'  -F 'user[password_confirmation]=secret123' -F 'user[locale]=en' #{MainYetting.HOST}/api/v1/users.json"
  def create

    ActiveRecord::Base.transaction do

      #recupera o endereço      
      if not params[:user][:chef_attributes].nil? and not params[:user][:chef_attributes][:kitchens_attributes].nil? and 
        not params[:user][:chef_attributes][:kitchens_attributes].empty? and 
        not params[:user][:chef_attributes][:kitchens_attributes][0][:address_attributes].nil? and
        not params[:user][:chef_attributes][:kitchens_attributes][0][:address_attributes][:zip_code].nil? then

        params_address = params[:user][:chef_attributes][:kitchens_attributes][0][:address_attributes]
        cep = params_address[:zip_code]

        address = Correios::CEP::AddressFinder.get(cep)
        if address.nil? then
          render :status => 200,
               :json => { :success => false,
                          :info => I18n.t(:msg_address_not_found)}
          return
        else
          if params_address[:street].nil? then
            params_address[:street] = address[:address]
          end
          params_address[:neighborhood] = address[:neighborhood]
          params_address[:city] = address[:city]
          params_address[:state] = address[:state]
          params_address[:country] = "Brasil"
        end
        
      end  
      
      build_resource(sign_up_params) 

      new_user = resource

      ###verifica se tem facebookID
      if not new_user.nil? and (new_user.facebook_id.nil? or new_user.facebook_id.blank?) then
        if new_user.save then
          save = true
        end
      else
        ### verifica se tem o registro para realizar o update
        user = User.where(:facebook_id => new_user.facebook_id).first
        if user.nil? then
          if new_user.save then
            save = true
          end
        else
          if update_resource(user, sign_up_params) then
            save = true
            new_user = user
          end
        end
      end
      if save then  
        if not params[:user][:user_push_tokens_attributes].nil? and not params[:user][:user_push_tokens_attributes].empty? then
          notification_service = BeMyChef::NotificationService.new
          params[:user][:user_push_tokens_attributes].each do |notification_param| 
            notification_service.save_push_notification(new_user, notification_param[:platform], notification_param[:token])
          end
        end
        render :status => 200,
               :json => { :success => true,
                          :user => new_user.as_json(:chef_kitchens => true, :exclude_token => true)}
      else
        render :status => :unprocessable_entity,
               :json => { :success => false,
                          :info => new_user.errors}
      end
    end
  end

  api :PUT, "/api/v1/users.json", "Altera um usuário [User/Chef]"
  description "Altera um  usuário"
  example "curl -F \"user[photo_attributes][file_content_type]=image/png\" -F \"user[photo_attributes][file]=@profile.png\" -F \"user[chef]=true\" -F \"user[chef_about]=teste chef about\" -F \"user[name]=nome usuário\"  -F \"user[email]=user22@example.com\"  -F \"user[password]=secret123\"  -F \"user[password_confirmation]=secret123\"  -F \"user[address_attributes][street]=rua tuim\" -F \"user[address_attributes][number]=356\" -F \"user[address_attributes][neighborhood]=Vila Uberabimha\" -F \"user[address_attributes][zip_code]=04514-100\" -F \"user[address_attributes][city]=São Paulo\" -F \"user[address_attributes][state]=São Paulo\" -F \"user[address_attributes][country]=Brasil\" -F \"user[locale]=en\"  -X PUT #{MainYetting.HOST}/api/v1/users.json"
  param_group :user
  param "User-Token", String, :required => false, :desc => "Token do usuário. Deve ser passado no Header"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  def update
    # required for settings form to submit when password is left blank
    if account_update_params[:password].blank?
      [:password,:password_confirmation,:current_password].collect{|p| account_update_params.delete(p) }
    end
    if account_update_params[:password].present? and account_update_params[:current_password].blank?
      [:current_password].collect{|p| account_update_params.delete(p) }
    end

    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    if update_resource(resource, account_update_params)
      yield resource if block_given?
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      
      if not params[:user][:user_push_tokens_attributes].nil? and not params[:user][:user_push_tokens_attributes].empty? then
        notification_service = BeMyChef::NotificationService.new
        params[:user][:user_push_tokens_attributes].each do |notification_param| 
          notification_service.save_push_notification(resource, notification_param[:platform], notification_param[:token])
        end
      end
        
      sign_in resource_name, resource, bypass: true

      render :status => 200,
             :json => { :success => true,
                        :info => "Updated",
                        :user => resource.as_json(:chef_kitchens => true) }

    else
      clean_up_passwords resource
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => resource.errors }
    end
  end
  
  api :POST, "/api/v1/users/recoverable.json", "Envia o email com as instruções para resetar a senha [User/Chef]"
  param :email, String, :required => true, :desc => "Email do usuário"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."  
  example "curl -F \"email=ranieripieper@gmail.com\" #{MainYetting.HOST}/api/v1/users/recoverable.json"
  def recoverable
    email = params[:email]

    if email.nil? then
      render :status => 200,
         :json => { :success => false,
                      :info => I18n.t(:msg_invalid_parameters)}
    else
      user = User.find_by_email(email)
      if user.nil? then
        render :status => 200,
          :json => { :success => false,
                      :info => I18n.t(:msg_entity_not_found)}
      else 
        #verficia se usuário já confirmou
        if not user.confirmed? then
          render :status => 200,
            :json => { :success => false,
                        :info => I18n.t(:msg_user_not_confirmed)}
        else
          user.send_reset_password_instructions
          render :status => 200,
                 :json => { :success => true, 
                            :info => I18n.t(:msg_send_reset_password_instructions, user_name: user.name, user_email: user.email)}
        end
      end
    end 
  end
  
  def_param_group :user_push_token do
    param :user_push_token, Hash, :required => false, :action_aware => true do
      param :token, String, :required => false, :desc => "Token"
      param :platform, ["ios", "android"], :required => true, :desc => "Plataforma - Valores válidos: ios e android"
    end
  end
  
  api :POST, "/api/v1/users/save_token.json", "Insere ou altera o token para push notification [User/Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param_group :user_push_token, :desc => "Push notification token"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."  
  example "curl -H 'User-Token: 12345' -F 'user_push_token[token]=xxx' -F 'user_push_token[platform]=ios' #{MainYetting.HOST}/api/v1/users/save_token.json"
  example "curl -H 'User-Token: 12345' -F 'user_push_token[token]=yyy' -F 'user_push_token[platform]=android' #{MainYetting.HOST}/api/v1/users/save_token.json"
  def save_push_token
    if params[:user_push_token].nil? or params[:user_push_token][:platform].nil? or
      ("ios" != params[:user_push_token][:platform] and "android" != params[:user_push_token][:platform]) then
       render :status => 200,
                :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
       return 
    end
    user = current_user
    
    notification_service = BeMyChef::NotificationService.new
    notification_service.save_push_notification(user, params[:user_push_token][:platform], params[:user_push_token][:token])

    render :status => 200,
            :json => { :success => true}
  end
  
  protected

  def update_resource(resource, params)
    resource.update_attributes(params)
  end
  
  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :locale, :photo, :photo_content_type, 
    :password_confirmation, :facebook_token, :facebook_id,
    #user_push_tokens_attributes: [:token, :platform],
    photo_attributes: [:file, :file_content_type],
    chef_attributes: [:about, bank_account_info_attributes: [:name, :bank, :agency, :current_account, :document], 
          kitchens_attributes: [:food_truck, :phone, 
                address_attributes: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country], 
                photos_attributes: [:file, :file_content_type]]])
  end
  
  def account_update_params
    params.require(:user).permit(:name, :email, :password, :locale, :photo, :photo_content_type, 
    :password_confirmation, :facebook_token, :facebook_id,
    #user_push_tokens_attributes: [:token, :platform],
    photo_attributes: [:file, :file_content_type],
    chef_attributes: [:about, bank_account_info_attributes: [:name, :bank, :agency, :current_account, :document]])
    
    
    #params.require(:user).permit(:password, :photo, :locale, :password_confirmation, :name)
  end
    
protected
  
  def set_csrf_header
     response.headers['X-CSRF-Token'] = form_authenticity_token
  end
end