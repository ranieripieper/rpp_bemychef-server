class Api::V1::IngredientsController < Api::V1::ApiController
  
  resource_description do
    formats ['json']
  end

  api :GET, "/api/v1/ingredients/get_all.json", "Retorna todos os ingredientes  [User/Chef]"
  param :page, :number, :required => false, :desc => "Número da página - Deve ser maior ou igual a 1. <br>Se o parâmetro não for passado, irá retornar todos os ingredientes"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/ingredients/get_all.json"
  example "#{MainYetting.HOST}/api/v1/ingredients/get_all.json?page=1"
  def get_all
    page = params[:page]
    ingredients = nil
    if not page.nil? and not page.empty? then
      ingredients = Ingredient.select("id, name, name_pt").paginate(:page => page)
    else
      ingredients = Ingredient.select("id, name, name_pt").all
    end

    render :status => 200,
             :json => { :success => true,
                        :ingredients => ingredients}
  end

  api :GET, "/api/v1/ingredients/get_dish_type.json", "Retorna o tipo de prato dependendo dos ingredients [Chef]"
  param :ingredients, Array, :required => true, :desc => "Lista dos ingredients"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/ingredients/get_dish_type.json?ingredients[]=1&ingredients[]=2"
  def get_dish_type
    ingredients_param = params[:ingredients]
    
    if ingredients_param.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else
    
      ingredient = Ingredient.select("sum(animal) as animal , sum(lacto) as lacto, sum(egg) as egg ")
                              .where("id in (?)", params[:ingredients]).first
  
      if ingredient.animal and ingredient.lacto and ingredient.egg then
        render :status => 200,
                 :json => { :success => true, :dish_type => nil} 
      elsif not ingredient.animal and not ingredient.lacto and not ingredient.egg then
        render :status => 200,
                 :json => { :success => true, :dish_type => {:id => MainYetting.DISH_TYPE_VEGETARIANO, :desc => I18n.t(:dish_type_vegano)}}
      elsif not ingredient.animal and ingredient.lacto and not ingredient.egg then
        render :status => 200,
                 :json => { :success => true, :dish_type => {:id => MainYetting.DISH_TYPE_LACTO, :desc => I18n.t(:dish_type_lacto)}}
      elsif not ingredient.animal and not ingredient.lacto and ingredient.egg then
        render :status => 200,
                 :json => { :success => true, :dish_type => {:id => MainYetting.DISH_TYPE_EGG, :desc => I18n.t(:dish_type_egg)}}
      elsif not ingredient.animal and ingredient.lacto and ingredient.egg then
        render :status => 200,
                 :json => { :success => true, :dish_type => {:id => MainYetting.DISH_TYPE_LACTO_EGG, :desc => I18n.t(:dish_type_lacto_egg)}}
      else
        render :status => 200,
                 :json => { :success => true, :dish_type => nil} 
      end  
    end  
  end

end