class Api::V1::ChefsController < Api::V1::ApiController

  before_action :authenticate_user!, only: [:get_payment_resume]

  resource_description do
    formats ['json']
  end

  api :GET, "/api/v1/chef/info/:chef_id.json", "Retorna as informações do chef pelo ID do chef [User]"
  param :chef_id, Integer, :required => true, :desc => "Id do Chef"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/chef/info/1.json"
  def get_chef_by_id
    chef_id = params[:chef_id].to_i
    
    service = BeMyChef::ChefService.new(params)
    chef = service.get_chef_by_id(chef_id)
     
    render :status => 200,
           :json => { :success => true, 
                      :chef => chef.as_json(:user => true, :kitchen => true)}
  
  end

  api :GET, "/api/v1/user/chef/:user_id.json", "Retorna as informações do chef pelo ID do usuário (chef) [User]"
  param :user_id, Integer, :required => true, :desc => "Id do usuário Chef"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/user/chef/1.json"
  def get_chef_by_user_id
    user_id = params[:user_id].to_i
    
    service = BeMyChef::ChefService.new(params)
    chef = service.get_chef_by_user_id(user_id)

    render :status => 200,
           :json => { :success => true, 
                      :chef => chef.as_json(:user => true, :kitchen => true)}
   
  end
  
  #retorna o resumo de pagamento do chef
  api :GET, "/api/v1/chef/payment/resume.json", "Retorna quanto o chef já recebeu e quanto o chef tem a receber [Chef]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/chef/payment/resume.json"
  def get_payment_resume
    user = get_user()
    
    service = BeMyChef::ChefService.new(params)
    value_paid = service.get_value_paid(user.id)
    value_not_paid = service.get_value_not_paid(user.id)
    
    render :status => 200,
                 :json => { :success => true, :resume => 
                                                { :value_paid => value_paid, 
                                                  :value_not_paid => value_not_paid }}
  end

end