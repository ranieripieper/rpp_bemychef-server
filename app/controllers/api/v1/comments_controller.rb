class Api::V1::CommentsController < Api::V1::ApiController
  
  before_action :authenticate_user!, only: [:create, :update, :get_comment_by_dish_user]

  resource_description do
    formats ['json']
  end

  def_param_group :photo_attributes do
    param :photo_attributes, Hash, :required => false, :action_aware => true do
      param :file, String, :required => false, :desc => "Foto"
      param :file_content_type, String, :required => false, :desc => "Content Type da foto"
    end
  end
  
  def_param_group :comment do
    param :comment, Hash, :required => true, :action_aware => true do
      param :comment, String, :required => true, :desc => "Comentário"
      param :rating, Integer, :required => true, :desc => "Rating"
      param_group :photo_attributes
    end
  end
  
  api :POST, "/api/v1/comments/:ordered_id/:dish_id/create.json", "Insere um novo comentário [User]"
  param "User-Token", String, :optional => true, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  param :dish_id, Integer, :required => true, :desc => "Id do prato"
  param_group :comment
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example 'curl -F "locale=pt-BR" -F "comment[photo_attributes][file_content_type]=image/jpg" -F "comment[photo_attributes][file]=@profile.jpg"  -F "comment[comment]=comentário 123" -F "comment[rating]=2"  #{MainYetting.HOST}/api/v1/comments/2/1/create.json'
  def create
    dish_id = params[:dish_id].to_i
    ordered_id = params[:ordered_id].to_i
    user_id = get_user_id()
    
    service = BeMyChef::CommentService.new(params)
    comment = service.create(get_user, ordered_id, dish_id, params)
     
    render :status => 201,
           :json => { :success => true, 
                      :chef => comment.as_json(:current_user_id => user_id)}
  end

  api :PUT, "/api/v1/:comment_id.json", "Altera um comentário [User]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :comment_id, Integer, :required => true, :desc => "Id do comentário"
  param_group :comment
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example 'curl -F "comment[photo_attributes][file_content_type]=image/jpg" -F "comment[photo_attributes][file]=@profile.jpg"  -F "comment[comment]=comentário 123" -F "comment[rating]=2"  #{MainYetting.HOST}/api/v1/1.json'
  def update
    comment_id = params[:comment_id].to_i
    user_id = get_user_id()
    
    service = BeMyChef::CommentService.new(params)
    comment = service.update(get_user, comment_id, params)

    if comment.nil? then
      render  :status => 403,
            :json => { :success => false, :info => service.errors}
    else
      render  :status => 200,
            :json => { :success => true, :comments => comment.as_json(:current_user_id => user_id)}
    end
    
  end
  
  api :GET, "/api/v1/dishes/:dish_id/comments.json", "Retorna os comentários do prato [User/Chef]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :dish_id, Integer, :required => true, :desc => "Id do prato"
  param :page, Integer, :required => false, :desc => "Pagina"
  param :per_page, Integer, :required => false, :desc => "Número de registros por página - Default: #{MainYetting.DEFAULT_REG_PER_PAGE}"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  example "#{MainYetting.HOST}/api/v1/dishes/1/comments.json?page=1&locale=pt-BR&nr_reg=3"
  def get_comments
    dish_id = params[:dish_id]
    page = params[:page]
    per_page = params[:per_page]

    dishes = nil
    
    service = BeMyChef::CommentService.new(params)
    comments = service.get_comments(dish_id, page, per_page)
      
    render :status => 200,
     :json => { :success => true,
                :comments => comments.as_json(:user => true, :current_user_id => get_user_id())}
                    
  end
 
  api :POST, "/api/v1/dishes/:dish_id/comments.json", "Retorna os comentários do prato [User/Chef]"
  def get_comments
    puts params[:username]
    puts params[:password]
  end
  
=begin
  api :GET, "/api/v1/dishes/:dish_id/my_comment.json", "Retorna o comentário do prato [User]"
  param :dish_id, Integer, :required => true, :desc => "Id do prato"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/dishes/1/my_comment.json?locale=pt-BR"
  def get_comment_by_dish_user
    dish_id = params[:dish_id]
    dishes = nil
    if dish_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      
      comment = Comment
                        .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `comments`.`photo_id` ')       
                        .where(:dish_id => dish_id, :user_id => get_user_id())
                        .order("comments.created_at desc").first
      
      render :status => 200,
       :json => { :success => true,
                  :comment => comment.as_json(:current_user_id => get_user_id())}
                    
    end
  end
=end
private
  def comment_params
    params.require(:comment).permit(:approved, :comment, :rating, photo_attributes: [:file, :file_content_type])
  end 
end