class Api::V1::AddressesController < Api::V1::ApiController

  before_action :authenticate_user!, only: [:get_addresses_delivery]

  resource_description do
    api_version "v1"
    formats ['json']
  end
  
  api :GET, "/api/v1/user/addresses/:cep.json", "Retorna o endereço com base no CEP  [User/Chef]"
  param "cep", String, :required => true, :desc => "CEP a ser consultado [Passado na URL]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/user/addresses/04087005.json"
  example "#{MainYetting.HOST}/api/v1/user/addresses/04087-005.json"
  def get_address_by_cep
    cep = params[:cep]
    
    service = BeMyChef::AddressesService.new(params)
    address = service.get_address_by_cep(cep)
    render :status => 200,
         :json => { :success => true,
                    :address => address}
  end
  
  api :GET, "/api/v1/user/addresses.json", "Retorna os endereços em que o Usuário já recebeu entregas [User]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/user/addresses.json"
  def get_addresses_delivered

   service = BeMyChef::AddressesService.new(params)
   render :status => 200,
         :json => { :success => true,
                    :addresses => service.get_addresses_delivered(get_user_id())}
  end
 
end