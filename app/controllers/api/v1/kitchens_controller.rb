class Api::V1::KitchensController < Api::V1::ApiController
  include UserHelper

  after_filter :set_csrf_header, only: [:update, :create]

  before_action :authenticate_user!, only: [:open_kitchen, :get_kitchens, :get_first_kitchen, :close_kitchen, :update_kitchen]
  
  resource_description do
    api_version "v1"
    formats ['json']
  end

  def_param_group :dish do
    param :dishes_attributes, Array, :required => true, :action_aware => true do
      param :id, Integer, :required => true, :desc => "Id do prato"
      param :price, Float, :required => true, :desc => "Preço do prato"
      param :quantity, Integer, :required => true, :desc => "Quantidade a ser vendida"
      param :time_to_prepare, Integer, :required => true, :desc => "Tempo de preparo em minutos"
    end
  end
  
  def_param_group :kitchens_open do
    param :kitchen, Hash, :required => true, :action_aware => true do
      param :delivery, ["true", "false", true, false], :required => true, :desc => "Chef faz a entrega do prato"
      param :pick_up, ["true", "false", true, false], :required => true, :desc => "Usuário retira o prato"
      param_group :dish, :desc => "Pratos"
    end
  end
  
  api :POST, "/api/v1/chef/kitchens/:kitchen_id/open.json", "Abre a cozinha [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha [parâmetro na URL]"
  param_group :kitchens_open, :desc => "Kitchens"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "curl -H 'User-Token: 12345' -X POST #{MainYetting.HOST}/api/v1/chef/kitchens/2/open.json"
  def open_kitchen
    user = get_user()
    kitchen_id = params[:kitchen_id].to_i
    #verifica se a cozinha é do chef logado
    kitchen = Kitchen.joins(:chef).where("user_id = ? and kitchens.id = ? ", user.id, kitchen_id).lock(true).first
    if kitchen.nil? then
      render  :status => 200,
              :json => { :success => false,
              :info => I18n.t(:msg_chef_dont_have_this_kitchen)}
      return
    end
    #só abre a cozinha se a cozinha já foi verificada
    if not kitchen.verified? then
      render  :status => 200,
              :json => { :success => false,
              :info => I18n.t(:msg_kitchen_not_verified)}
      return
    end
    #só abre a cozinha se a cozinha está fechada

    if not kitchen.close then
      render  :status => 200,
              :json => { :success => false,
              :info => I18n.t(:msg_kitchen_opened)}
      return
    end

    #adiciona os valores nos params para o update
    params[:kitchen][:close] = false

    params[:kitchen][:dishes_attributes].each do |param_dish| 
      param_dish[:available] = true
      param_dish[:quantity_sale] = 0
      param_dish[:quantity_reserved] = 0
    end
    
    if kitchen.update_attributes(params_open_kitchen) then
      
      kitchen = Kitchen
                  .joins([{:chef => [ :user ] }])
                  .joins(:dishes)
                  .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
                  .joins('LEFT OUTER JOIN `ordereds` ON `dishes`.`id` = `ordereds`.`dish_id` ')
                  .includes([{:chef => [:user ]}, :dishes])
                  .where("chefs.user_id = ? and kitchens.id = ? and dishes.available = ? ", user.id, kitchen_id, true).first
                  
      render  :status => 200,
              :json => { :success => true,
              :kitchen => kitchen.as_json(:dish => true, :chef => true)}
    else
      render  :status => 200,
              :json => { :success => false,
              :info => I18n.t(:msg_erro_open_kitchen),
              :debug => kitchen.errors}
    end
  end

  def params_open_kitchen
    params.require(:kitchen).permit(:delivery, :pick_up, :close,
      dishes_attributes: [:id, :price, :quantity, :time_to_prepare, :available])
  end
  
  api :GET, "/api/v1/kitchens/:kitchen_id.json", "Retorna as informações da cozinha pelo ID [User/Chef]"
  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/kitchens/1.json"
  def get_kitchen_by_id
    kitchen_id = params[:kitchen_id].to_i

    if kitchen_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      kitchen = Kitchen.joins([{:chef => [ :user ] }])
                        .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
                        .includes([{:chef => [:user ]}, :dishes])
                        .find(kitchen_id)
      
      if kitchen.nil? then
        render :status => 200,
                 :json => { :success => false,
                            :info => I18n.t(:msg_entity_not_found)}
      else
        
        render :status => 200,
                 :json => { :success => true, :kitchens => kitchen.as_json(:dish => true, :chef => true)}
      
      end
    end
  end
  
  
  api :GET, "/api/v1/chef/kitchen.json", "Retorna a cozinha do chef logado [Chef] - Usado para a primeira fase - Chef com uma cozinha"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/chef/kitchen.json"
  def get_first_kitchen
    #método usado na versão 1 - quando o chef so possui uma cozinha
    user_id = get_user().id
    kitchen = Kitchen
            .joins([{:chef => [{:user => :photo}]}])
            .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
            .joins('LEFT OUTER JOIN `dishes` ON `dishes`.`kitchen_id` = `kitchens`.`id` and dishes.deleted_at is null ')
            .joins('LEFT OUTER JOIN `photos` `photos_kit` ON `kitchens`.`id` = `photos_kit`.`kitchen_id` and dishes.excluded = 0')
            .includes(:photos, :dishes)
            .includes([{:chef => [ {:user => :photo}]}])
            .where("users.id = ? ", user_id).first
            
    ordereds = Ordered
                .joins(:dish => [:kitchen] )
                .includes(:dish)
                .where("kitchens.id = ?", kitchen.id)
                .limit(20)
    render :status => 200,
           :json => { :success => true, 
                      :kitchen => kitchen.as_json(:dish => true, :chef => true, :photos => true),
                      :ordereds => ordereds.as_json(:only_dish => true)}
                 
    
  end
  
  api :GET, "/api/v1/chef/kitchens.json", "Retorna as cozinhas do chef logado [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/chef/kitchens.json"
  def get_kitchens
    kitchens = get_kitchens_by_user_id(get_user().id)
        
    render  :status => 200,
              :json => { :success => true, :kitchens => kitchens.as_json(:dish => true)}
  end
  
  api :GET, "/api/v1/user/chef/:user_chef_id/kitchens.json", "Retorna as cozinhas pelo id do Usuário (Chef) [User]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :only_avaiable_dish, [true, false], :required => false, :desc => "Retorna só os pratos disponíveis para venda no momento. Só existem pratos disponíveis para a venda, se a cozinha está aberta (close = false) <br> Valor Default: false"
  example "#{MainYetting.HOST}/api/v1/user/chef/3/kitchens.json"
  example "#{MainYetting.HOST}/api/v1/user/chef/3/kitchens.json?only_avaiable_dish=true"
  def get_kitchens_by_user_chef_id
    user_chef_id = params[:user_chef_id].to_i

    if user_chef_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      kitchens = get_kitchens_by_user_id(user_chef_id, params[:only_avaiable_dish].to_b)
        
      render  :status => 200,
              :json => { :success => true, :kitchens => kitchens.as_json(:dish => true)}
    end    
  end
  
  api :GET, "/api/v1/chef/:chef_id/kitchens.json", "Retorna as cozinhas pelo id do Chef [User]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :only_avaiable_dish, [true, false], :required => false, :desc => "Retorna só os pratos disponíveis para venda no momento. Só existem pratos disponíveis para a venda, se a cozinha está aberta (close = false) <br> Valor Default: false"
  example "#{MainYetting.HOST}/api/v1/chef/3/kitchens.json"
  example "#{MainYetting.HOST}/api/v1/chef/3/kitchens.json?only_avaiable_dish=true"
  def get_kitchens_by_chef
    chef_id = params[:chef_id].to_i

    if chef_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      kitchens = get_kitchens_by_chef_id(chef_id, params[:only_avaiable_dish].to_b)
        
      render  :status => 200,
              :json => { :success => true, :kitchens => kitchens.as_json(:dish => true)}
    end    
  end

  api :POST, "/api/v1/chef/kitchens/:kitchen_id/close.json", "Fecha a cozinha [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha"
  example "curl  -H 'User-Token: 12345' -X POST #{MainYetting.HOST}/api/v1/chef/kitchens/2/close.json"
  def close_kitchen
    kitchen_id = params[:kitchen_id].to_i
    if kitchen_id.nil? then
       render :status => 200,
                :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else
      ActiveRecord::Base.transaction do
        kitchen = Kitchen.lock(true).joins(:chef => [:user]).where("users.id = ? and close = ? and kitchens.id = ?", get_user().id, false, kitchen_id).first
        
        if kitchen.nil? then
          render :status => 200,
                  :json => { :success => false,
                      :info => I18n.t(:msg_kitchen_not_found_or_closed)}
        else
          service = BeMyChef::ChefService.new
          service.close_kitchen(kitchen)

           render :status => 200,
                  :json => { :success => true,
                      :info => I18n.t(:msg_kitchen_closed_success)}
        end
      end
    end
  end
  
  api :DELETE, "/api/v1/chef/kitchens/:kitchen_id/photo/:photo_id", "Exclui uma foto da cozinha [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha"
  param :photo_id, Integer, :required => true, :desc => "Id da foto"
  example "curl  -H 'User-Token: 12345' -X DELETE #{MainYetting.HOST}/api/v1/chef/kitchens/1/photo/4.json"
  def delete_photo
    kitchen_id = params[:kitchen_id].to_i
    photo_id = params[:photo_id].to_i
    ActiveRecord::Base.transaction do
      kitchen_exist = Kitchen.joins(:chef => [:user]).where("users.id = ? and kitchens.id = ?", get_user().id, kitchen_id).count
      
      if kitchen_exist <= 0 then
        render :status => 200,
                :json => { :success => false,
                    :info => I18n.t(:msg_kitchen_not_found_or_not_belong)}
      else
        Photo.delete_all(id: photo_id, kitchen_id: kitchen_id)
        
        render :status => 200,
                 :json => { :success => true,
                            :info => I18n.t(:msg_kitchen_photo_deleted)}

      end
    end
  end
  
  
  def_param_group :address do
    param :address_attributes, Hash, :required => true, :action_aware => true do
      param :street, String, :required => false, :desc => "Rua"
      param :number, String, :required => true, :desc => "Número"
      param :complement, String, :required => false, :desc => "Complemento"
      param :zip_code, String, :required => true, :desc => "CEP"
      param :country, String, :required => false, :desc => "País"
    end
  end
    
  def_param_group :kitchens_update do
    param :kitchen, Hash, :required => true, :action_aware => true do
      param :delivery, ["true", "false"], :required => true, :desc => "Chef faz a entrega do prato"
      param :pick_up, ["true", "false"], :required => true, :desc => "Usuário retira o prato"
      param :phone, String, :required => true, :desc => "Telefone da cozinha"
      param_group :address, :desc => "Endereço"
    end
  end

  api :PUT, "/api/v1/chef/kitchens/:kitchen_id", "Fecha a cozinha [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :kitchen_id, Integer, :required => true, :desc => "Id da cozinha"
  param_group :kitchens_update, :desc => "Kitchens"
  example "curl  -H 'User-Token: 12345' -F 'kitchen[phone]=123456' -X PUT #{MainYetting.HOST}/api/v1/api/v1/chef/kitchens/1"
  def update_kitchen
    kitchen_id = params[:kitchen_id].to_i
    ActiveRecord::Base.transaction do
      kitchen = Kitchen.lock(true).joins(:chef => [:user]).where("users.id = ? and kitchens.id = ?", get_user().id, kitchen_id).first
      
      if kitchen.nil? then
        render :status => 200,
                :json => { :success => false,
                    :info => I18n.t(:msg_kitchen_not_found_or_not_belong)}
      else
        if kitchen.update_attributes(update_kitchen_params) then  
           render :status => 200,
                   :json => { :success => true,
                              :kitchen => kitchen.as_json(:address => true, :photos => true)}
        else
          render :status => :unprocessable_entity,
                 :json => { :success => false,
                            :info => kitchen.errors}
        end
      end
    end
  end

  def update_kitchen_params
    params.require(:kitchen).permit(:phone, 
                address_attributes: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country], 
                photos_attributes: [:file, :file_content_type])
  end
  
  ## Comando de teset para criar a cozinha
  #curl -F "user[chef]=true" -F "user[chef_about]=teste chef about" -F "user[device_id]=ios_1234" -F "user[name]=nome usuário"  -F "user[email]=user22@example.com"  -F "user[password]=secret123"  -F "user[password_confirmation]=secret123" -F "user[address_attributes][street]=rua tuim" -F "user[address_attributes][number]=356" -F "user[address_attributes][neighborhood]=Vila Uberabimha" -F "user[address_attributes][zip_code]=04514-100" -F "user[address_attributes][city]=São Paulo" -F "user[address_attributes][state]=São Paulo" -F "user[address_attributes][country]=Brasil" #{MainYetting.HOST}/api/v1/users.json
  api!
  def create

    ActiveRecord::Base.transaction do

      build_resource(sign_up_params) 

      #verifica parâmetros
      if not params[:user][:chef].nil? and params[:user][:chef].to_b then
        if params[:user][:chef_about].nil? then
          render :status => :unprocessable_entity,
               :json => { :success => false,
                          :info => new_user.errors}
        end
            
      end
          
      new_user = resource

      ###verifica se tem facebookID
      if not new_user.nil? and (new_user.facebook_id.nil? or new_user.facebook_id.empty?) then
        if new_user.save then
          save = true
        end
      else
        ### verifica se tem o registro para realizar o update
        user = User.where(:facebook_id => new_user.facebook_id).first
        if user.nil? then
          if new_user.save then
            save = true
          end
        else
          if update_resource(user, sign_up_params) then
            save = true
            new_user = user
          end
        end
        
      end
  
      if save then      
        
        if not params[:user][:chef].nil? and params[:user][:chef].to_b then
          chef = Chef.where(:users_id => new_user.id).first  
          if chef.nil? then
            chef = Chef.new
          end
          chef.users_id = new_user.id
          chef.about = params[:user][:chef_about]
          chef.save
        end
        
        sign_in new_user
        
        render :status => 200,
               :json => { :success => true,
                          :user => new_user}
      else
        render :status => :unprocessable_entity,
               :json => { :success => false,
                          :info => new_user.errors}
      end
    end
  end

private 
  def get_kitchens_by_user_id(user_id, only_avaiable_dish = false) 
    if not only_avaiable_dish then
      Kitchen.joins([{:chef => [{ :user => :photo }]}])
              .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
              .joins('LEFT OUTER JOIN `dishes` ON `dishes`.`kitchen_id` = `kitchens`.`id` and dishes.deleted_at is null')
              .includes([{:chef => [{ :user => :photo }]}, :dishes])
              .where("users.id = ?", user_id)
    else
      Kitchen.joins([{:chef => [{ :user => :photo }]}])
              .joins('LEFT OUTER JOIN `dishes` ON `dishes`.`kitchen_id` = `kitchens`.`id` and dishes.deleted_at is null')
              .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
              .includes([{:chef => [{ :user => :photo }]}, :dishes])
              .where("users.id = ? and dishes.available = ?", user_id, true)
    end
  end

  def get_kitchens_by_chef_id(chef_id, only_avaiable_dish = false) 
    if not only_avaiable_dish then
      Kitchen.joins([{:chef => [{ :user => :photo }]}, :dishes])
              .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
              .includes([{:chef => [{ :user => :photo }]}, :dishes])
              .where("chefs.id = ?", chef_id)
    else
      Kitchen.joins([{:chef => [{ :user => :photo }]}, :dishes])
              .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
              .includes([{:chef => [{ :user => :photo }]}, :dishes])
              .where("chefs.id = ? and dishes.available = ?", chef_id, true)
    end
  end
end