class Api::V1::UsersController < Api::V1::ApiController

  before_action :authenticate_user!, only: [:get_user_info]

  resource_description do
    formats ['json']
  end

  
  api :GET, "/api/v1/users/me.json", "Retorna as informações do usuário logado [User/Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example 'curl  #{MainYetting.HOST}/api/v1/users/me.json'
  def get_user_info
    user = get_user
    render :status => 200,
         :json => { :success => true,
                    :user => user.as_json(:chef_kitchens => true, :exclude_token => true) }
  end

end