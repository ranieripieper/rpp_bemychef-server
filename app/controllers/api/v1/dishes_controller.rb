class Api::V1::DishesController < Api::V1::ApiController
  include PaymentHelper
  include OrderedHelper
  include UserHelper

  before_action :authenticate_user!, only: [:insert_dish, :delete_dish]

  resource_description do
    api_version "v1"
    formats ['json']
  end

  def_param_group :dish_ingredients_attributes do
    param :dish_ingredients_attributes, Array, :required => true, :action_aware => true do
      param :ingredient_id, Integer, :required => true, :desc => "Id do ingrediente"
    end
  end

  def_param_group :photo do
    param :photos_attributes, Array, :required => false, :action_aware => true do
      param :file, ActionDispatch::Http::UploadedFile, :required => true, :desc => "Arquivo"
    end
  end  
  
  def_param_group :dish do
    param :dish, Hash, :required => true, :action_aware => true do
      param :name, String, :required => true, :desc => "Nome"
      param :description, String, :required => true, :desc => "Descrição"
      param :dish_type, ["0", "1", "2", "3", "4"], :required => false, :desc => "Tipo do prato. (0) nenhum, (1) Vegetariano, Vegano, (2) Ovo-lacto-vegetarianos, (3) Ovo-vegetarianos, (4) Lacto-vegetarianos"
      param :organic, ["true", "false"], :required => true, :desc => "Se o prato é orgânico"
      param :price, Float, :required => true, :desc => "Preço"
      param :quantity, Integer, :required => false, :desc => "Quantidade a ser vendida"
      param :time_to_prepare, Integer, :required => true, :desc => "Tempo de preparo em minutos"
      param :kitchen_id, Integer, :required => false, :desc => "Id da cozinha. Se for passado null, irá usar a primeira cozinha. Usado na versão 1.0 quando só é possível ter uma cozinha por chef"
      param_group :dish_ingredients_attributes, :desc => "Ingredientes"
      param_group :photo, :required => false, :desc => "Fotos"
    end
  end
  
  api :POST, "/api/v1/dishes/insert.json", "Insere um novo prato [Chef]"
  param_group :dish, :desc => "Prato"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "curl  -H 'User-Token: 12345' -F 'dish[dish_ingredients_attributes][][ingredient_id]=1' -F 'dish[dish_ingredients_attributes][][ingredient_id]=2'  -F 'dish[name]=teste' -F 'dish[description]=desc%20prato' -F 'dish[quantity]=10' -F 'dish[price]=10.90' -F 'dish[organic]=false' -F 'dish[dish_type]=1' -F 'dish[time_to_prepare]=20' -F 'dish[kitchen_id]=' -F 'User-Token=12345' -F 'dish[photos_attributes][][file_content_type]=image/jpg' -F 'dish[photos_attributes][][file]=@bmc_logo_share.png' -F 'dish[photos_attributes][][file_content_type]=image/jpg' -F 'dish[photos_attributes][][file]=@dds_logo.png' #{MainYetting.HOST}/api/v1/dishes/insert.json"
  def insert_dish
    user = get_user()
    if params[:dish][:kitchen_id].nil? || params[:dish][:kitchen_id].blank? then
      #recupera a cozinha do usuário
      kitchen = Kitchen.joins(:chef).where("user_id = ? ", user.id).first
      if kitchen.nil? then
        # "erro - chef não tem nenhuma cozinha"
        render :status => 200,
          :json => { :success => false,
                      :info => I18n.t(:msg_chef_havent_kitchen)}
        return
      else
        params[:dish][:kitchen_id] = kitchen.id
      end
    else
      #verifica se a cozinha é do usuário
      kitchen = Kitchen.joins(:chef).where("user_id = ? and kitchens.id = ? ", user.id, params[:dish][:kitchen_id].to_i).first
      if kitchen.nil? then
        # "erro - cozinha não pertence ao usuário"
        render :status => 200,
            :json => { :success => false,
                        :info => I18n.t(:msg_chef_dont_have_this_kitchen)}
        return
        
      end
    end
    dish = Dish.new(dish_insert_params)
    if dish.save then
      render :status => 200,
            :json => { :success => true,
                       :dish => dish.as_json(:only_dish => true)}
    else
      render :status => 200,
            :json => { :success => false,
                        :info => I18n.t(:msg_error_insert_dish),
                        :debug => dish.errors}
    end    
  end
  
  def dish_insert_params
    params.require(:dish).permit(:name, :description, :dish_type, :organic, :price, :quantity, :time_to_prepare, :kitchen_id,
    dish_ingredients: [:ingredient_id], photos_attributes: [:file, :file_content_type])
  end
  
  api :DELETE, "/api/v1/dishes/:dish_id/delete.json", "Exclui um prato [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "curl -H 'User-Token: 12345' -F 'dish[dish_ingredients_attributes][][ingredient_id]=1' -F 'User-Token=123456' -X DELETE #{MainYetting.HOST}/api/v1/dishes/1/delete.json"
  def delete_dish
    user = get_user()
    
    if params[:dish_id].nil? or params[:dish_id].blank? then
      render :status => 200,
             :json => { :success => false,
             :info => I18n.t(:msg_invalid_parameters)}
      return
    end
    dish_id = params[:dish_id].to_i
    #verifica se o prato é do chef logado
    dish = Dish.joins(:kitchen => [:chef]).where("chefs.user_id = ? and dishes.id = ? ", user.id, dish_id).first
    if dish.nil? then
      render :status => 200,
             :json => { :success => false,
             :info => I18n.t(:msg_chef_dont_have_this_dish)}
      return
    else  

      #verifica se o prato tem pedidos. Se tiver faz exclusão lógica
      if dish.ordereds.exists? or dish.reserves.exists? or dish.comments.exists? then
        dish.destroy
      else
        dish.destroy!
      end
      if dish.errors.nil? or dish.errors.empty? then
        render :status => 200,
               :json => { :success => true,
                          :info=> I18n.t(:msg_success_delete_dish)}
      else 
        render :status => 200,
               :json => { :success => false, 
                          :info=> I18n.t(:msg_error_delete_dish),
                          :debug => dish.errors}
      end
    end    
  end
  
  api :GET, "/api/v1/dishes/:dish_id.json", "Retorna o prato pelo ID [User/Chef]"
  param :dish_id, Integer, :required => true, :desc => "Id do prato"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/dishes/1.json"
  def get_by_dish_id
    dish_id = params[:dish_id]
    dishes = nil
    if dish_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 

      dishes = Dish
                    .joins(:ingredients)
                    .joins('LEFT OUTER JOIN `photos` ON `dishes`.`id` = `photos`.`dish_id` ')
                    .includes(:ingredients, :photos)
                    .find(dish_id.to_i)

      render :status => 200,
             :json => { :success => true,
                        :dish => dishes.as_json(:photos => true, :ingredients => true)}

    end
 
  end
  
  api :GET, "/api/v1/dishes/find_near.json", "Retorna os pratos que estão disponíveis no momento na região [User]"
  param :page, :number, :required => true, :desc => "Número da página - Deve ser maior ou igual a 1."
  param :lat, Float, :required => true, :desc => "Latitude"
  param :lng, Float, :required => true, :desc => "Longitude"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/dishes/find_near.json?page=1&lat=-23.6121&lng=-46.6627"
  def find_near
    lat = params[:lat]
    lng = params[:lng]
    page = params[:page]
    dishes = nil
    if (lat.nil? or lng.nil?) or page.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 

      dishes = Dish
                    .select("(
                      3959 * acos (cos ( radians(#{lat}) ) * cos( radians( addresses.latitude ) ) * cos( radians( addresses.longitude ) - radians(#{lng}) )
                      + sin ( radians(#{lat}) )
                      * sin( radians( addresses.latitude ) )
                    )) AS distance")
                    .joins(kitchen: [{ chef: [:user] }, :address])
                    .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
                    .includes(kitchen: [{ chef: [:user] }, :address])
                    .where("kitchens.close = ? and dishes.available = ?", false, true)
                    .having("distance < ?", MainYetting.DEFAULT_MILES_FIND_NEAR)
                    .order("distance")
                    .paginate(:page => page)

      render :status => 200,
             :json => { :success => true,
                        :dishes => dishes.as_json(:kitchen_address => true, :chef => true, :photos => true, :ingredients => true)}

    end
 
  end

  def_param_group :dish_update do
    param :dish, Hash, :required => true, :action_aware => true do
      param :quantity, Integer, :required => false, :desc => "Quantidade a ser vendida"
      param :time_to_prepare, Integer, :required => false, :desc => "Tempo de preparo em minutos"
      param :price, Float, :required => false, :desc => "Preço do prato"
    end
  end
  
  api :PUT, "/api/v1/dishes/:dish_id/update.json", "Faz update do prato. Pode alterar quantidade, tempo de preparo e preço [Chef]"
  param :dish_id, Integer, :required => false, :desc => "Id do prato"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param_group :dish_update
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/dishes/1/update.json?User-Token=123&dish[quantity]=101&dish[time_to_prepare]=45&dish[price]=30.00"
  def update_dish
    dish_id = params[:dish_id].to_i
    #verifica se o prato é do chef
    dish = Dish.lock(true).joins(:kitchen).where("dishes.id = ? and kitchens.chef_id = ?", dish_id, get_user_id()).first
    if dish.nil? then
      entity_not_found()
    else
      if dish.update_attributes dish_update_params then
        render :status => 200,
             :json => { :success => true,
                        :dishes => dish.as_json(:base => true)}
      else
      end
    end
 
  end
  
private
  def dish_update_params
    params.require(:dish).permit(:quantity, :time_to_prepare, :price)
  end 
end