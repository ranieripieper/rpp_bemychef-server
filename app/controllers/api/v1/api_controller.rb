module Api
  module V1

    class ApiController  < ActionController::API
      include UserHelper

      before_filter :set_locale, :authenticate_user_from_token!


      # ParamError is superclass of ParamMissing, ParamInvalid
      rescue_from Apipie::ParamError do |e|
        render :status => 400,
             :json => { :success => false,
                        :sign_in => false,
                        :info =>  I18n.t(:msg_invalid_parameters),
                        :error => e.message}
      end
  
      def set_locale
        I18n.locale = params[:locale]  if params[:locale]
      end
  
      resource_description do
        formats ['json']
        api_version "v1"
      end
      
      
      def get_user
        return current_user
      end

      def get_user_id
        if current_user.nil? then
          nil
        else
          current_user.id
        end
      end

      def render_user_not_signed
        render :status => 401,
             :json => { :success => false,
                        :sign_in => true,
                        :info =>  I18n.t(:msg_sign_in_required)}
      end
      
      rescue_from ActiveRecord::RecordNotFound, with: :entity_not_found
      rescue_from ActionController::RoutingError, with: :register_already_exist
    
      def entity_not_found
        render :status => 404,
                     :json => { :success => false,
                                :info => I18n.t(:msg_entity_not_found)}
      end
      
      def register_already_exist
        render :status => 403,
                     :json => { :success => false,
                                :info => I18n.t(:msg_entity_already_exist)}
      end

    def authenticate_user_from_token!
      
      user_token = "#{request.headers['User-Token']}"
  
      if user_token.nil? or user_token.empty? then
        user_token = params['User-Token'].presence
      end
      
      #user_token = params[:user_token].presence
      if user_token then
        #pos_id = user_token.index(":")
        #id = user_token[0..pos_id-1]
        #token = user_token[pos_id+1..user_token.size]
        user = User.find_by_authentication_token(user_token)
      end
     
   
      if user
        # Notice we are passing store false, so the user is not
        # actually stored in the session and a token is needed
        # for every request. If you want the token to work as a
        # sign in token, you can simply remove store: false.
        sign_in user, store: false
      end
      
      log_additional_data()
    end
    end
  end
end
