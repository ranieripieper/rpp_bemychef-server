class Api::V1::OrderedsController < Api::V1::ApiController
  
  before_action :authenticate_user!, only: [:get_history_ordereds, :get_user_history_ordereds, :cancel_reserve, :reserve_dish, :get_addresses_delivery, :confirm_reserve, :accept_ordered, :ordered_ready, :get_ordered_by_id]

  resource_description do
    api_version "v1"
    formats ['json']
  end
   
  def_param_group :reserve do
    param :reserve, Hash, :required => true, :action_aware => true do
      param :quantity, Integer, :required => true, :desc => "Quantidade de pratos"
      param :delivery_pick_up, ["1", "2", "3", 1, 2, 3], :required => true, :desc => "se o chef/motoboy faz a entrega ou se o usuário retira. Valores válidos: 1 -> Chef Delivery - 2 -> Usuário retira - 3 -> Motoboy delivery"
      param :ordered_value, Float, :required => true, :desc => "Valor do pedido (somente o prato - qt x valor do prato). Este campo é usado somente para validar com o backend se o valor está correto do informado para o usuário."
      param :delivery_price, Float, :required => true, :desc => "Valor do delivery. Este campo é usado somente para validar com o backend se o valor está correto do informado para o usuário."
    end
  end
  
  def reserve_params
    params.require(:reserve).permit(:dish_id, :quantity, :delivery_pick_up, :ordered_value, :delivery_price) 
  end
  
  api :POST, "/api/v1/ordereds/dishes/reserve/:dish_id.json", "Faz a reserva do prato [User]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :dish_id, Integer, :required => true, :desc => "Id do prato [Passado na URL]"
  param_group :reserve, :desc => "Reserva"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "curl -H 'User-Token: 12345' -F 'reserve[quantity]=2' -F 'reserve[ordered_value]=20' -F 'reserve[delivery_price]=5' -F 'reserve[delivery_pick_up]=1'  -X POST #{MainYetting.HOST}/api/v1/ordereds/dishes/reserve/2.json"
  def reserve_dish

    user = get_user()
    dish = nil
    reserve = nil
    
    params[:reserve][:dish_id] = params[:dish_id].to_i
    reserve = Reserve.new(reserve_params)

    if reserve.dish_id.nil? or reserve.dish_id <= 0 or reserve.quantity.nil? or reserve.quantity <= 0 or reserve.ordered_value.nil? or reserve.delivery_price.nil? or 
      (reserve.delivery_pick_up.to_i <= 0 or reserve.delivery_pick_up.to_i > 3) then
      render :status => 200,
             :json => { :success => false,
                        :info => I18n.t(:msg_invalid_parameters)}
      return
    end
    
    #verifica se já tem um pedido aguardando confirmação do chef
    if OrderedHelper::has_ordered_in_process(user.id, reserve.dish_id) then
      render :status => 200,
             :json => { :success => false,
                        :ordered_in_process => true,
                        :info => I18n.t(:msg_ordered_in_process)}
      return
    end
    begin
      ActiveRecord::Base.transaction do
        dish = Dish.joins(:kitchen).includes(:kitchen).lock(true).find(reserve.dish_id)
        ## verifica a cozinha está aberta
        if dish.kitchen.close then
          render :status => 200,
                 :json => { :success => false,
                            :kitchen_close => true,
                            :ordered_in_process => false,
                            :info => I18n.t(:msg_kitchen_closed)}
          return
        end
        
        service = BeMyChef::OrderedsService.new
        
        #verifica se tem reserva para este usuário e prato, se tiver cancela
        service.verify_and_cancel_reserves(user.id, dish)
  
        #verifica valor
        value = reserve.total_value
        total_value = service.get_ordered_price(dish, reserve.quantity, reserve.delivery_pick_up)

        if total_value != value then
          render :status => 200,
                 :json => { :success => false,
                            :kitchen_close => false,
                            :ordered_in_process => false,
                            :invalid_value => true,
                            :info => I18n.t(:msg_error_ordered_price),
                            :dish => dish.as_json(:only_dish => true),
                            :debug => {:backend_value => total_value, :request_total_value => value} }
          return
        end
        #verifica se prato está disponível
        if not dish.available then
          render :status => 200,
                 :json => { :success => false,
                            :kitchen_close => false,
                            :ordered_in_process => false,
                            :invalid_value => false,
                            :info => I18n.t(:msg_dish_unavailable),
                            :dish => dish.as_json(:only_dish => true) }
          return
        end
        #verifica quantidade disponível
        if (dish.quantity - dish.quantity_sale - dish.quantity_reserved) < reserve.quantity then
          render :status => 200,
                 :json => { :success => false,
                            :kitchen_close => false,
                            :ordered_in_process => false,
                            :invalid_value => false,
                            :info => I18n.t(:msg_invalid_quantity),
                            :dish => dish.as_json(:only_dish => true) }
          return
        end
        #salva a reserva do prato
        reserve.canceled = false
        reserve.confirmed = false
        reserve.user_id = user.id
        dish.quantity_reserved = dish.quantity_reserved + reserve.quantity
        if reserve.save! and dish.save! then
          render :status => 200,
                 :json => { :success => true,
                            :kitchen_close => false,
                            :ordered_in_process => false,
                            :info => I18n.t(:msg_success_reserve),
                            :dish => dish.as_json(:only_dish => true),
                            :reserve => reserve.as_json() }
        else           
            raise ActiveRecord::Rollback
          
            render :status => 200,
                   :json => { :success => false,
                              :kitchen_close => false,
                              :ordered_in_process => false,
                              :info => I18n.t(:msg_ocorreu_erro),
                              :dish => dish.as_json(:only_dish => true),
                              :debug => {:reserve => reserve.errors, :dish => dish.errors } }
        end                 
      end # end transaction
    rescue  => e
      puts e
      Rails.logger.payment.error("Erro ao reservar o prato. Dish: #{params[:reserve][:dish_id]} User: #{get_user().id} - Erro: => #{e.message}")
    
      render :status => 200,
             :json => { :success => false,
                        :kitchen_close => false,
                        :ordered_in_process => false,
                        :info => I18n.t(:msg_ocorreu_erro),
                        :dish => dish.nil? ? nil : dish.as_json(:only_dish => true),
                        :debug => {:reserve => reserve.nil? ? nil : reserve.errors, :dish => dish.nil? ? nil : dish.errors } }
    end
  end  

  api :DELETE, "/api/v1/ordereds/reserve/:reserve_id/cancel.json", "Cancela a reserva do prato [User]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :reserve_id, Integer, :required => true, :desc => "Id da reserva [Passado na URL]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "curl -H 'User-Token:12345' -X POST #{MainYetting.HOST}/api/v1/ordereds/reserve/14/cancel.json"
  def cancel_reserve
    begin
      ActiveRecord::Base.transaction do
        reserves = Reserve.joins(:dish).includes(:dish).lock(true).where(:id => params[:reserve_id].to_i, :canceled => false, :confirmed => false)
        if not reserves.nil? and not reserves.empty? then
          reserves.each do |reserve|
            reserve.canceled = true
            reserve.dish.quantity_reserved = reserve.dish.quantity_reserved - reserve.quantity
            reserve. save
            reserve.dish.save
          end          
        end
        render :status => 200,
               :json => { :success => true,
                          :info => I18n.t(:msg_reserve_canceled) }
      end
    rescue  => e
      puts e
      Rails.logger.payment.error("Erro ao cancelar a reserva. Reserva #{params[:reserve_id]} User: #{get_user().id} - Erro: => #{e.message}")
    
      render :status => 200,
             :json => { :success => false,
                        :info => I18n.t(:msg_reserve_not_canceled) }
    end
  end

  def_param_group :address do
    param :address, Hash, :required => true, :action_aware => true do
      param :street, String, :required => false, :desc => "Rua"
      param :number, String, :required => true, :desc => "Número"
      param :complement, String, :required => false, :desc => "Complemento"
      param :zip_code, String, :required => true, :desc => "CEP"
      param :country, String, :required => false, :desc => "País"
    end
  end
  
  def address_params
    params.require(:address).permit(:street, :number, :complement, :zip_code, :country)
  end
  
  api :POST, "/api/v1/ordereds/reserve/:reserve_id/pagar.me/confirm.json", "Realiza o pedido do prato [User]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :reserve_id, Integer, :required => true, :desc => "Id da reserva"
  param :card_hash, String, :required => true, :desc => "Hash dos dados do cartão - pagar.me"
  param_group :address
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "curl -H 'User-Token: 12345' -F 'card_hash=xyaz1234' -X POST #{MainYetting.HOST}/api/v1/ordereds/reserve/1/pagar.me/confirm.json"
  def confirm_reserve_pagar_me

    #service = BeMyChef::ProcessPaymentPagarMeService.new(get_user(), params)
    
    #service.process
    
    #render :status  => service.status,
    #       :json    => service.json_response

    value = 1000
    
    PagarMe.api_key = "ak_test_Bdb8iQ6ZqdRYdTifOLiRsZxivmiBqT";
    card_hash = params[:card_hash]
    transaction = PagarMe::Transaction.new({
        :amount => value,
        :card_hash => "#{card_hash}",
        
        #:capture => false,
        :soft_descriptor => MainYetting.PAGAR_ME_SOFT_DESCRIPTION,
        :metadata => {
            :user_id => 123,
            :ordered_id => 456
        },
        :capture => "false"
    })
    
    transaction.charge
  
    puts transaction
  
    render :status  => 200,
          :json    => transaction,
          :html   => "xxxx"
  end
  
  
  api :POST, "/api/v1/ordereds/reserve/:reserve_id/confirm.json", "Realiza o pedido do prato [User]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :reserve_id, Integer, :required => true, :desc => "Id da reserva"
  param :card_holder_name, String, :required => true, :desc => "Nome do titular do cartão"
  param :card_number, String, :required => true, :desc => "Número do cartão"
  param :card_cvv, Integer, :required => true, :desc => "Código de segurança do cartão"
  param :card_exp_year, Integer, :required => true, :desc => "Ano de expiração do cartão"
  param :card_exp_month, Integer, :required => true, :desc => "Mês de expiração do cartão"
  param :card_flag, String, :required => true, :desc => "Bandeira do cartão"
  param_group :address
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "curl -H 'User-Token: 12345' -F 'card_holder_name=Ranieri' -F 'card_number=4012001037141112' -F 'card_cvv=123' -F 'card_exp_year=2018' -F 'card_exp_month=05' -F 'card_flag=visa' -X POST #{MainYetting.HOST}/api/v1/ordereds/reserve/1/confirm.json"
  def confirm_reserve
    reserve_id = params[:reserve_id].to_i

    card_holder_name = params[:card_holder_name]
    card_number = params[:card_number]
    card_cvv = params[:card_cvv]
    card_exp_year = params[:card_exp_year]
    card_exp_month = params[:card_exp_month]
    card_flag = params[:card_flag]

    reserve = nil
    
    if reserve_id.nil? or card_holder_name.nil? or card_number.nil? or
      card_cvv.nil? or card_exp_year.nil? or card_exp_month.nil? or card_flag.nil?  then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else
    
      begin
        transaction_id = nil
        payment_status = nil
        user = get_user()
 
        ActiveRecord::Base.transaction do
          reserve = Reserve.joins(:dish).includes(:dish).lock(true).where(:id => reserve_id, :user_id => user.id).first

          if reserve.nil? or reserve.canceled then
            render :status => 200,
                   :json => { :success => false,
                              :refresh_dish => true,
                              :info => I18n.t(:msg_invalid_reserve),
                              :reserve => reserve.as_json() }
            return
          end

          qt = reserve.quantity
          delivery_pick_up = reserve.delivery_pick_up
          address = nil
          dish = reserve.dish
          
          #verifica o endereço 
          if reserve.delivery_pick_up != MainYetting.ORDERED_PICK_UP then 
            if params[:address].nil? then
              render :status => 200,
                       :json => { :success => false,
                                  :address_required => true,
                                  :info => I18n.t(:msg_address_delivery_required)}
              return
            end        
            address = Address.new(address_params)
            if not address.nil? and not address.zip_code.nil? then
              address_correios = Correios::CEP::AddressFinder.get(address.zip_code)
              if address_correios.nil? then
                render :status => 200,
                       :json => { :success => false,
                                  :address_required => true,
                                  :info => I18n.t(:msg_address_delivery_required)}
                return
              end
              if address.street.nil? then
                address.street = address_correios[:address]
              end
              address.neighborhood = address_correios[:neighborhood]
              address.city = address_correios[:city]
              address.state = address_correios[:state]
              address.country = "Brasil"
              address.save
            end
          else
            address = dish.kitchen.address
          end
    
          #verifica se já tem um pedido aguardando confirmação do chef
          if OrderedHelper::has_ordered(user.id, reserve.dish_id, MainYetting.ORDERED_STATUS_ORDERED) then
            render :status => 200,
                  :json => { :success => false,
                      :info => I18n.t(:msg_ordered_in_process)}
            return
          end

          #cria o pedido
          ordered = OrderedHelper::create_ordered(user, dish, reserve, address)
          #realiza o pagamento - não realiza a captura
          puts "process pgto"
          payment = PaymentHelper::process_payment(user.id, ordered.id, reserve.dish_id, card_holder_name, card_number, card_cvv, card_exp_year, card_exp_month, card_flag, reserve.total_value)
          puts "...."
          transaction_id = payment.transaction_id
          status = payment.status
          
          if not payment.payment_approved then
            render :status => 200,
                 :json => { :success => false,
                      :info => I18n.t(:msg_payment_not_approved),
                      :dish => dish.as_json(:only_dish => true) }
            
            raise ActiveRecord::Rollback
            
          else
            #confirma a reserva e diminui o nr. de pratos disponíveis
            reserve.canceled = false
            reserve.confirmed = true
            reserve.save
            
            dish.quantity_sale = dish.quantity_sale + reserve.quantity
            dish.quantity_reserved = dish.quantity_reserved - reserve.quantity
            dish.save

            #envia notificação para o chef
            BeMyChef::NotificationService.new.notify_ordered(dish, ordered)
            
            #schedule task para cancelar o pedido automaticamente
            scheduler = Rufus::Scheduler.new
            scheduler.in "#{MainYetting.TIMEOUT_ACCEPT_ORDERED}s" do
              puts "**** Cancelando pedido automaticamente"
              OrderedHelper::ordered_timeout(ordered.id)
            end
          end          
          
          render :status => 200,
                 :json => { :success => true,
                            :info => I18n.t(:msg_ordered_send),
                            :dish => dish.as_json(:only_dish => true),
                            :ordered => ordered}
        end # fim da transação
        
      rescue  => e
        puts e
        
        if not reserve.nil? then
          reserve.attempts = reserve.attempts + 1
          if MainYetting.ORDERED_ATTEMPTS <= reserve.attempts then
            reserve.canceled = true
          end
          reserve.save
        end        
        
        Rails.logger.payment.error("Erro ao processar pagamento. User: #{get_user().id} - transaction_id: #{transaction_id} - Erro: => #{e.message}")
      
        PaymentHelper::cancel_cielo_payment(transaction_id, status, e.message, nil)
        
        render :status => 200,
                   :json => { :success => false,
                        :info => I18n.t(:msg_internal_error)}
      end   
    end 
  end
  
  api :POST, "/api/v1/ordereds/:ordered_id/accept.json", "Confirma ou não o pedido do prato [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  param :accepted, ["true", "false", true, false], :required => true, :desc => "true -> Chefe aceitou o pedido - False -> Chefe não aceitou o pedido"
  example "#{MainYetting.HOST}/api/v1/ordereds/1/accept.json?accepted=true"
  example "#{MainYetting.HOST}/api/v1/ordereds/1/accept.json?accepted=false"
  def accept_ordered
    accepted = params[:accepted]
    ordered_id = params[:ordered_id].to_i

    if ordered_id.nil? or accepted.nil? or
      ("true" != accepted and "false" != accepted) then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      accepted = accepted.to_b
      ActiveRecord::Base.transaction do
        
        user = get_user()
        
        #verifica se o usuário é o chef "dono do prato"
        ordered = Ordered.lock(true).joins(:dish => [:kitchen => [:chef]]).where("`ordereds`.`id` = ? and `chefs`.`user_id` = ?", ordered_id, user.id).first
        if ordered.nil? then
          render :status => 200,
                 :json => { :success => false,
                            :info => I18n.t(:msg_ordered_not_found)}
          return
        else

          #verifica o status do pedido
          ordered_status = OrderedStatus.where("ordered_id = ?", ordered_id).first
          if MainYetting.ORDERED_STATUS_ORDERED != ordered_status.status_id then
             render :status => 200,
                   :json => { :success => false,
                              :info => I18n.t(:msg_incompatible_ordered_status)}
             return
          else 

            #confirma ou não o pagamento
            payment = Payment.where("ordered_id = ?", ordered_id).first
            payment.can_capture = accepted
            payment.save
            
            #update status ordered
            ordered_status = accepted ? MainYetting.ORDERED_STATUS_ACCEPTED : MainYetting.ORDERED_STATUS_NOT_ACCEPTED

            OrderedHelper::update_ordered_status(ordered, ordered_status) 
              
            #create status
            if accepted then
              #verifica se tem que fechar a cozinha
              close_kitchen = true
              kitchen = Kitchen.joins(:dishes).includes(:dishes).lock(true).where(:id => ordered.dish.kitchen_id).first
              if not kitchen.nil? and not kitchen.dishes.nil? then
                kitchen.dishes.each do |dish|
                  if dish.available and dish.quantity == dish.quantity_sale then
                    dish.available = false
                    dish.save
                  else
                    close_kitchen = false
                  end
                end
              end
              if close_kitchen then
                service = BeMyChef::ChefService.new
                service.close_kitchen(kitchen)
              end
              notification_service = BeMyChef::NotificationService.new
              notification_servicenotify_accepted_ordered(user, ordered) 
              render :status => 200,
                     :json => { :success => true,
                                :close_kitchen => close_kitchen,
                                :info => I18n.t(:msg_ordered_accepted)}
              return
             else
               #cancela pagamento
               PaymentHelper::cancel_payment(payment.transaction_id, nil, I18n.t(:msg_ordered_not_accepted), ordered.id)
               notification_servicenotify_not_accepted_ordered(user, ordered) 
               rollback_ordered(ordered)
               
               chef = user.chef 
               chef.qt_not_accept_ordered = chef.qt_not_accept_ordered + 1
               chef.save
               
               render :status => 200,
                     :json => { :success => true,
                                :info => I18n.t(:msg_ordered_not_accepted)}
             end
           end
         end
      end # fim da transação
    end 
  end


  api :POST, "/api/v1/ordereds/:ordered_id/cancel_by_chef.json", "Cancela o pedido [Chef]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  example "#{MainYetting.HOST}/api/v1/ordereds/1/cancel_by_chef.json"
  def cancel_ordered_by_chef
    ordered_id = params[:ordered_id].to_i

    if ordered_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      ActiveRecord::Base.transaction do
        
        user = get_user()
        
         #verifica se o usuário é o chef "dono do prato"
        ordered = Ordered.joins(:dish => [:kitchen => [:chef]]).where("`ordereds`.`id` = ? and `chefs`.`user_id` = ?", ordered_id, user.id).first
        if ordered.nil? then
          render :status => 200,
                 :json => { :success => false,
                            :info => I18n.t(:msg_ordered_not_found)}
        else

          #verifica o status do pedido
          ordered_status = OrderedStatus.where("ordered_id = ?", ordered_id).first
          if MainYetting.ORDERED_STATUS_ACCEPTED != ordered_status.status_id then
             render :status => 200,
                   :json => { :success => false,
                              :info => I18n.t(:msg_incompatible_ordered_status)}
          else 

            #cancela o pagamento
            payment = Payment.where("ordered_id = ?", ordered_id).first
            payment.can_capture = false
            payment.save
            PaymentHelper::cancel_cielo_payment(payment.transaction_id, payment.status, I18n.t(:msg_ordered_cancel_by_chef), ordered.id)
            
            #update status ordered
            OrderedHelper::update_ordered_status(ordered, MainYetting.ORDERED_STATUS_CANCEL_CHEF) 
            
            rollback_ordered(ordered)
            
            chef = user.chef 
            chef.qt_cancel_ordered = chef.qt_cancel_ordered + 1
            chef.save
               
            BeMyChef::NotificationService.new.notify_ordered_cancel_by_chef(user, ordered) 
               
               render :status => 200,
                     :json => { :success => true,
                                :info => I18n.t(:msg_ordered_cancel_by_chef)}
           end
         end
      end # fim da transação

    end
 
  end
  
  api :POST, "/api/v1/ordereds/:ordered_id/ordered_ready.json", "Prato está pronto para entrega/retirada [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  example "#{MainYetting.HOST}/api/v1/ordereds/1/ordered_ready.json"
  def ordered_ready
    ordered_id = params[:ordered_id].to_i

    if ordered_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      ActiveRecord::Base.transaction do
        
        user = get_user()
        
         #verifica se o usuário é o chef "dono do prato"
        ordered = Ordered.joins(:dish => [:kitchen => [:chef]]).where("`ordereds`.`id` = ? and `chefs`.`user_id` = ?", ordered_id, user.id).first
        if ordered.nil? then
          render :status => 200,
                 :json => { :success => false,
                            :info => I18n.t(:msg_ordered_not_found)}
        else

          #verifica o status do pedido
          ordered_status = OrderedStatus.where("ordered_id = ?", ordered_id).first
          if MainYetting.ORDERED_STATUS_ACCEPTED != ordered_status.status_id then
             render :status => 200,
                   :json => { :success => false,
                              :info => I18n.t(:msg_incompatible_ordered_status)}
          else 

            #update status ordered
            OrderedHelper::update_ordered_status(ordered, MainYetting.ORDERED_STATUS_COOKED) 
            
            BeMyChef::NotificationService.new.notify_ordered_ready(user, ordered) 
               render :status => 200,
                     :json => { :success => true,
                                :info => I18n.t(:msg_chef_ordered_ready)}
           end
         end
      end # fim da transação
    end
  end

  api :GET, "/api/v1/ordereds/:ordered_id.json", "Retorna as informações do pedido [User/Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  example "#{MainYetting.HOST}/api/v1/ordereds/66.json"
  def get_ordered_by_id
    ordered_id = params[:ordered_id].to_i

    if ordered_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      user = get_user()
      ordered = Ordered.unscoped
                      .joins([{:dish => [{ kitchen: [:chef => :user] }]}, :address] )
                      .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
                      .includes([{:dish => [{ kitchen: [chef: [:user => :photo]] }]}, :address] )
                      .find_by_id(ordered_id)
      if ordered.nil? then
        render :status => 200,
                 :json => { :success => false,
                            :info => I18n.t(:msg_ordered_not_found)}
      else
        #verifica se o pedido é do usuário ou chef
        if ordered.user_id == user.id or ordered.dish.kitchen.chef.user_id == user.id then
          render :status => 200,
                   :json => { :success => true,
                              :ordered => ordered.as_json(:dish => true, :address => true, :user => true, :status_history => true)}
        else
          render :status => 200,
                   :json => { :success => false,
                              :info => I18n.t(:msg_ordered_not_belong_user_chef)}
        end
      end
    end
  end

 
   api :GET, "/api/v1/user/ordereds.json", "Retorna todos os pedidos paginados [User]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :page, :number, :required => false, :desc => "Número da página - Deve ser maior ou igual a 1."
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/user/ordereds.json"
  def get_user_history_ordereds
    page = params[:page]
    if page.nil? then
      page = 1
    end
    ordereds = Ordered.joins(:dish => [:kitchen => [:chef]]).where("`ordereds`.`user_id` = ?", get_user().id)
                        .order("ordereds.updated_at desc")
                        .paginate(:page => page)
  
    render :status => 200,
                 :json => { :success => true, :ordereds => ordereds.as_json(:chef_payment => true, :only_dish => true)}
  end
  
  api :GET, "/api/v1/ordereds.json", "Retorna todos os pedidos paginados [Chef]"
  param "User-Token", String, :required => false, :desc => "Token do usuário - Deve ser passado no Cabeçalho"
  param :page, :number, :required => false, :desc => "Número da página - Deve ser maior ou igual a 1."
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  example "#{MainYetting.HOST}/api/v1/ordereds.json"
  def get_history_ordereds
    page = params[:page]
    if page.nil? then
      page = 1
    end
    ordereds = Ordered.joins(:dish => [:kitchen => [:chef]]).where("`chefs`.`user_id` = ?", get_user().id)
                        .order("ordereds.updated_at desc")
                        .paginate(:page => page)
  
    render :status => 200,
                 :json => { :success => true, :ordereds => ordereds.as_json(:chef_payment => true, :only_dish => true)}
  end
  
private
  #Retorna a quantidade do pedido para ser vendida
  def rollback_ordered(ordered)
    dish = Dish.lock(true).find(ordered.dish)
    dish.quantity_sale = dish.quantity_sale - ordered.quantity
    dish.save
  end
  
=begin 
  api :POST, "ordereds/:ordered_id/cancel_by_user.json", "Cancela o pedido [User]"
  param :locale, String, :required => false, :desc => "Idioma. Ex.: en, pt-BR..."
  param :ordered_id, Integer, :required => true, :desc => "Id do pedido"
  example "#{MainYetting.HOST}/api/v1/ordereds/1/cancel_by_user.json"
  def cancel_ordered_by_user
    ordered_id = params[:ordered_id].to_i

    if ordered_id.nil? then
      render :status => 200,
       :json => { :success => false,
                    :info => I18n.t(:msg_invalid_parameters)}
    else 
      ActiveRecord::Base.transaction do
        
        user = get_user()
        
         #verifica se o usuário é o chef "dono do prato"
        ordered = Ordered.where("`ordereds`.`id` = ? and `user_id` = ?", ordered_id, user.id).first
        if ordered.nil? then
          render :status => 200,
                 :json => { :success => true,
                            :info => I18n.t(:msg_ordered_not_found)}
        else

          #verifica o status do pedido
          ordered_status = OrderedStatus.where("ordered_id = ?", ordered_id).first
          if MainYetting.ORDERED_STATUS_ACCEPTED != ordered_status.status_id then
             render :status => 200,
                   :json => { :success => true,
                              :info => I18n.t(:msg_incompatible_ordered_status)}
          else 

            #cancela o pagamento
            payment = Payment.where("ordered_id = ?", ordered_id).first
            payment.can_capture = false
            payment.save
            cancel_payment(payment.transaction_id, payment.status, I18n.t(:msg_ordered_cancel_by_chef), ordered.id)
            
            #update status ordered
            OrderedHelper::update_ordered_status(ordered, MainYetting.ORDERED_STATUS_CANCEL_USER) 
            
            BeMyChef::NotificationService.new.notify_ordered_cancel_by_user(user, ordered) 
               #TODO - a quantidade do pedido volta a ser vendida ou não? A cozinha fecha?
               render :status => 200,
                     :json => { :success => true,
                                :info => I18n.t(:msg_ordered_cancel_by_user)}
           end
         end
      end # fim da transação
    end 
  end
=end
end