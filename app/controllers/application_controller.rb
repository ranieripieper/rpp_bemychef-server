class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_locale
  skip_before_filter  :verify_authenticity_token

  def set_locale
    I18n.locale = params[:locale] if params[:locale] || I18n.default_locale
  end
      
  def landing
    puts ""
    @subscription = Subscription.new
    expires_in 24.hours, :public => true
    render "landing"
  end
  
  def subscription
    subscription = Subscription.new(subscription_params)
    @subscription = Subscription.new
    if subscription.save then
      redirect_to root_path(:success => true, :locale => I18n.locale)
    else
      @subscription = subscription
      @success = false
      render "landing"
    end
    
  end
  
  def resume
    nr_chef = 0
    nr_nao_chef = 0
    if params[:pw] == "dds&pgr2" then
      nr_chef = Subscription.where(:chef => 1).count
      nr_nao_chef = Subscription.where(:chef => 0).count
    end
    render :status => 200,
            :json => { :chefs => nr_chef,
                        :users => nr_nao_chef}
  end

  def subscription_params
    params.require(:subscription).permit(:name, :email, :phone, :country, :zip_code, :chef)
  end
  
end
