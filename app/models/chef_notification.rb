class ChefNotification < ActiveRecord::Base
  belongs_to :chef
  belongs_to :ordered
  
  default_scope -> { order("chef_notifications.updated_at desc") }
  
  def as_json(options = { })
    options = options.merge(default_options())
    if (not options[:ordered].nil? and options[:ordered]) then
      options = options.deep_merge(ordered_options())
    end

    h = super(options)
    h
  end
  
private
  def default_options
    {
      :format => :table,
      :only => [:id, :text, :notification_type, :created_at, :ordered_id, :chef_id]
    }
  end
  
  def ordered_options
    {
      :include => {
        :ordered => { 
          :only => [:id, :quantity, :delivery_pick_up, :ps, :dish_id, :user_id, :address_id, :last_ordered_status_id],
          :include => {
            :dish => {
              :only => [:id, :name, :description, :price, :time_to_prepare, :organic, :dish_type]
            }        
          }
        }
      }
    }
  end
end
