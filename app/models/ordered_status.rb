class OrderedStatus < ActiveRecord::Base
  belongs_to :ordered
  
  default_scope -> { order("updated_at desc") }
end
