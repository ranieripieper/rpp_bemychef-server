class Comment < ActiveRecord::Base
  belongs_to :photo
  belongs_to :dish
  belongs_to :user
  
  accepts_nested_attributes_for :photo
  
  self.per_page = MainYetting.DEFAULT_REG_PER_PAGE
  
  def comment_for_user
    if not current_user.nil? and current_user.id == user_id then
      true
    else
      false
    end
  end
  
  def as_json(options = { })
    options = options.merge(default_options())
    if (not options[:user].nil? and options[:user]) then
      options = options.deep_merge(user_options())
    end
    h = super(options)
    if not options[:current_user_id].nil? and options[:current_user_id] == user_id then
      h[:can_edit] = true
    else
      h[:can_edit] = false
    end
    h
  end
  
private
  def default_options
    {
      :format => :table,
      :only => [:id, :comment, :rating, :photo, :dish_id],
      :include => {
        :photo => {
              :only => [:id ],
              :methods => [:file_url]
            }
      }
    }
  end
  
  def user_options 
    {
      :include => {
        :user => {
          :only => [:id, :name, :email],
          :include => {
            :photo => {
              :only => [:id ],
              :methods => [:file_url]
            }
          }
        }      
      }
    }
  end
end
