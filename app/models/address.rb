class Address < ActiveRecord::Base
  
  has_many :ordereds
  def address
    [street, city, state, 'Brazil'].compact.join(', ')
  end

  geocoded_by :address
  after_validation :geocode          # auto-fetch coordinates

  def as_json(options = { })
    options = options.merge(default_options())

    h = super(options)

    h
  end

  def get_address
    addr = {street: self.street, number: self.number, complement: self.complement, neighborhood: self.neighborhood, zip_code: self.zip_code, city: self.city, state: self.state }
  end
  
private
  def default_options
    {
      :format => :table,
      :only => [:id, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :latitude, :longitude ]
    }
  end
end
