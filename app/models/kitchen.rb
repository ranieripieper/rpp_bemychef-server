class Kitchen < ActiveRecord::Base
  
  has_many :dishes
  has_many :photos
  belongs_to :chef
  belongs_to :address

  accepts_nested_attributes_for :address, update_only: true
  accepts_nested_attributes_for :photos
  accepts_nested_attributes_for :dishes

  def as_json(options = { })
    options = options.merge(default_options())
    if (not options[:dish].nil? and options[:dish]) then
      options = options.deep_merge(dish_options())
    end
    if (not options[:chef].nil? and options[:chef]) then
      options = options.deep_merge(chef_options())
    end
    if (not options[:photos].nil? and options[:photos]) then
      options = options.deep_merge(photos_options())
    end
    if (not options[:address].nil? and options[:address]) then
      options = options.deep_merge(address_options())
    end
    h = super(options)

    h
  end

private
  def default_options
    {
      :format => :table,
      :only => [:id, :close, :verified, :phone]
    }
  end
  
  def dish_options
    {
      :include => {
        :dishes => {
          :only => [:id, :name, :description, :price, :time_to_prepare, :organic, :dish_type],
          :methods => [:quantity_available],
          :include => {
            :photos => {
              :only => [:id ],
              :methods => [:file_url]
            }
          }
          }
       }
    }
  end
  
  def dish_with_actual_ordereds_options
    {
      :include => {
        :dishes => {
          :only => [:id, :name, :description, :price, :time_to_prepare, :organic, :dish_type],
          :methods => [:quantity_available],
          :include => {
              :ordereds => {
                :only => [:id, :quantity, :delivery_pick_up]
              }
          }
        }
      }
    }
  end
  
  def chef_options
    {
      :include => {
        :chef => {
          :only => [:id, :nr_rating, :avg_rating, :qt_not_accept_ordered, :qt_cancel_ordered],
          :include => {
            :user => {
              :only => [:id, :name, :email],
              :include => {
                :photo => {
                  :only => [:id ],
                  :methods => [:file_url]
                }
              }
            }
          }                            
        }
      }
    }
  end
  
  def address_options
    {
      :include => {
        :address => {
          :only => [:id, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :latitude, :longitude ]
        }
      }
    }
  end
  
  def photos_options
    {
      :include => {
        :photos => {
          :only => [:id ],
          :methods => [:file_url]
        }
      }
    }
  end
end
