class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # , :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  #relacionamentos
  has_one :chef
  belongs_to :photo
  has_many :user_push_tokens
  has_many :reserves, class_name: "Reserve"

  accepts_nested_attributes_for :photo
  accepts_nested_attributes_for :chef, update_only: true
  accepts_nested_attributes_for :user_push_tokens

  before_save :ensure_authentication_token

  def as_json(options = { })
    options = options.merge(default_options())
    if (not options[:chef].nil? and options[:chef]) then
      options = options.deep_merge(chef_options())
    end
    if (not options[:chef_kitchens].nil? and options[:chef_kitchens]) then
      options = options.deep_merge(chef_kitchens_options())
    end
    if (not options[:exclude_token].nil? and options[:exclude_token]) then
      options = options.deep_merge(exclude_token_options())
    else 
      options = options.deep_merge(include_token_options())      
    end

    h = super(options)
    if self.confirmed? and options[:exclude_token].nil? then
      h[:auth_token] = self.auth_token  
    end
    h
  end
 

  def confirmed
      self.confirmed?  
  end
  
  def get_parse_channel
    "#{MainYetting.PARSE_CHANNEL_PREFIX_USER}#{id}"
  end
  
  def ensure_authentication_token
    if auth_token.blank?
      self.auth_token = "#{generate_authentication_token}"
    end
  end
  
private

  def default_options
    {
      :format => :table,
      :only => [:id, :name, :email],
      :methods => [:confirmed],
      :include => {
        :photo => {
          :only => [:id ],
          :methods => [:file_url]
        }
      }
    }
  end
  
  def include_token_options
    {
      :only => [:id, :name, :email, :auth_token]
    }
  end
  
  def exclude_token_options
    {
      :only => [:id, :name, :email],
      :exclude => [:auth_token]
    }
  end
  
  def chef_options
    {
      :include => {
        :chef => {
          :only => [:id, :nr_rating, :avg_rating, :qt_not_accept_ordered, :qt_cancel_ordered]
        }
      }
    }
  end
  
  def chef_kitchens_options
    {
      :include => {
        :chef => {
          :only => [:id, :about, :nr_rating, :avg_rating, :qt_not_accept_ordered, :qt_cancel_ordered],
          :include => {
            :kitchens => {
              :only => [:id, :close, :verified],
              :include => {
                :address => {
                  :only => [:id, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :latitude, :longitude ]
                },
                :photos => {
                  :only => [:id ],
                  :methods => [:file_url]
                }
              }
            }
          }
        }
      }
    }
  end
  
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(auth_token: token).first
    end
  end
    
  class << self
    def find_by_authentication_token(token)
      return User.where(:auth_token => "#{token}").first
    end
  end
end
