class UserNotification < ActiveRecord::Base
  belongs_to :user
  belongs_to :ordered
  belongs_to :comment
  
  default_scope -> { order("user_notifications.updated_at desc") }
  
  def as_json(options = { })
    options = options.merge(default_options())
    if (not options[:ordered].nil? and options[:ordered]) then
      options = options.deep_merge(ordered_options())
    end

    h = super(options)
    h
  end
  
private
  def default_options
    {
      :format => :table,
      :only => [:id, :text, :notification_type, :created_at, :ordered_id, :chef_id]
    }
  end
  
  def ordered_options
    {
      :include => {
        :ordered => { 
          :only => [:id, :quantity, :delivery_pick_up, :ps, :dish_id, :user_id, :address_id, :last_ordered_status_id],
          :include => {
            :dish => {
              :only => [:id, :name, :description, :price, :time_to_prepare, :organic, :dish_type]
            }        
          }
        },
        :comment => {
          :only => [:id, :comment, :rating, :photo],
          :include => {
            :photo => {
                  :only => [:id ],
                  :methods => [:file_url]
                }
          }
        }
      }
    }
  end
end
