class Ordered < ActiveRecord::Base
  belongs_to :address
  belongs_to :dish
  belongs_to :user
  has_many :ordered_statuses
  
  default_scope -> { order("updated_at desc") }
  
  def total_value
    return delivery_price + ordered_value  
  end
  
  def as_json(options = { })
    options = options.merge(default_options())
    if (not options[:only_dish].nil? and options[:only_dish]) then
      options = options.deep_merge(only_dish_options())
    end
    if (not options[:dish].nil? and options[:dish]) then
      options = options.deep_merge(dish_options())
    end
    if (not options[:address].nil? and options[:address]) then
      options = options.deep_merge(address_options())
    end
    if (not options[:user].nil? and options[:user]) then
      options = options.deep_merge(user_options())
    end
    if (not options[:status_history].nil? and options[:status_history]) then
      options = options.deep_merge(status_history_options())
    end
    if (not options[:chef_payment].nil? and options[:chef_payment]) then
      options = options.deep_merge(chef_payment_options())
    end
    h = super(options)

    h
  end

private
  def default_options
    {
      :format => :table,
      :only => [:id, :quantity, :delivery_pick_up, :ps, :dish_id, :user_id, :address_id, :last_ordered_status_id, :dish_price, :delivery_price]
    }
  end

  def chef_payment_options
    {
      :format => :table,
      :only => [:id, :quantity, :delivery_pick_up, :ps, :dish_id, :user_id, :address_id, :last_ordered_status_id, :paid_for_chef, :dt_paid_for_chef]
    }
  end
 
  def only_dish_options
    {
      :include => {
        :dish => {
          :only => [:id, :name, :description, :price, :time_to_prepare, :organic, :dish_type],
          :include => {
            :photos => {
              :only => [:id ],
              :methods => [:file_url]
            }  
          }
       }        
      }
    }
  end
  
  def dish_options
    {
      :include => {
        :dish => {
          :only => [:id, :name, :description, :price, :time_to_prepare, :organic, :dish_type],
          :include => {
            :kitchen => { 
              :only => [:id, :close, :verified],
              :include => {
                :chef => {
                  :only => [:id, :nr_rating, :avg_rating, :qt_not_accept_ordered, :qt_cancel_ordered],
                  :include => {
                    :user => {
                      :only => [:id, :name, :email],
                      :include => {
                        :photo => {
                          :only => [:id ],
                          :methods => [:file_url]
                        }
                      }
                    }
                  }                            
                }
              }
            }
          }
       }        
      }
    }
  end
  
  def address_options
    {
      :include => {
                    :address => {
                      :only => [:id, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :latitude, :longitude ]
                   }
        
      }
    }
  end
  
  def user_options
    {
      :include => {
                    :user => {
                      :only => [:id, :name, :email ]
                   }
        
      }
    }
  end
  
  def status_history_options
    {
      :include => {
                    :ordered_statuses => {
                      :only => [:id, :status_id, :created_at ]
                   }
        
      }
    }
  end
end
