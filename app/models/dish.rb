class Dish < ActiveRecord::Base
  
  acts_as_paranoid
  
  has_many :comments, dependent: :destroy
  has_many :dish_ingredients, dependent: :destroy
  has_many :ingredients, through: :dish_ingredients
  has_many :photos, dependent: :destroy
  has_many :ordereds
  has_many :reserves, class_name: "Reserve"
  belongs_to :kitchen 
  
  accepts_nested_attributes_for :dish_ingredients
  accepts_nested_attributes_for :photos
  
  self.per_page = MainYetting.DEFAULT_REG_PER_PAGE

  def quantity_available
    if self.quantity > 0 and self.available then
      return self.quantity - self.quantity_sale
    else
      return 0
    end  
  end
  
  def as_json(options = { })
    
    options = options.merge(default_options())
    if (not options[:kitchen_address].nil? and options[:kitchen_address]) then
      options = options.deep_merge(kitchen_address_options())
    end 
    if (not options[:ingredients].nil? and options[:ingredients]) then
      options = options.deep_merge(ingredients_options())
    end
    if (not options[:chef].nil? and options[:chef]) then
      options = options.deep_merge(chef_options())
    end
    if (not options[:photos].nil? and options[:photos]) then
      options = options.deep_merge(photos_options())
    end
    
=begin
    if (not options[:only_dish].nil? and options[:only_dish]) then
      options = options.merge(only_dish_options())
    else
      options = options.merge(dish_options())
    end
=end
    h = super(options)

    h
  end

private
  def default_options
    {
      :format => :table,
      :only => [:id, :name, :description, :dish_type, :organic, 
        :delivery, :pick_up, :price, :time_to_prepare, :available, :quantity, :quantity_sale  ],
      :methods => [:quantity_available]
    }
  end
  
  def photos_options
    {
      :include => {
        :photos => {
          :only => [:id ],
          :methods => [:file_url]
        }
      }
    }
  end
      
  def ingredients_options
    {
      :include => {
        :ingredients => {
          :only => [:id, :name, :name_pt]
        }
      }
    }
  end
  
  def kitchen_address_options
  {
    :include => {
      :kitchen => {
        :only => [:id, :close, :verified, :delivery, :pick_up],
        :include => {
          :address => {
            :only => [:id, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :latitude, :longitude ],
           }
        }
      }
    }
  }
  end
  
  def chef_options
    {
      :include => {
        :kitchen => {
          :include => {
            :chef => {
              :only => [:id, :about, :avg_rating, :nr_rating, :qt_cancel_ordered, :qt_not_accept_ordered],
              :include => {
                :user => {
                  :only => [:id, :name ],
                  :include => {
                    :photo => {
                      :only => [:id ],
                      :methods => [:file_url]
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  end
  
  def dish_options
    {
      :format => :table,
      :only => [:id, :name, :description, :dish_type, :organic, :delivery, :pick_up, :price, :quantity, :quantity_sale, :time_to_prepare, :available  ],
      :include => {
        :ingredients => {
          :only => [:id, :name, :name_pt]
        },
        :kitchen => {
          :only => [:id, :close, :verified],
          :include => {
            :address => {
              :only => [:id, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :latitude, :longitude ],
            },
            :chef => {
              :only => [:id, :about, :avg_rating, :nr_rating, :qt_cancel_ordered, :qt_not_accept_ordered],
              :include => {
                :user => {
                  :only => [:id, :name ],
                  :include => {
                    :photo => {
                      :only => [:id ],
                      :methods => [:file_url]
                    }
                  }
                }
              }
          
            }
          }
       }        
      }
    }
  end
end
