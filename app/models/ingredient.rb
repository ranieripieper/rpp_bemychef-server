class Ingredient < ActiveRecord::Base
  
  self.per_page = 50
  
  has_many :dish_ingredients
  has_many :dishes, through: :dish_ingredients
end
