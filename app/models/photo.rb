class Photo < ActiveRecord::Base
  
  belongs_to :kitchen
  belongs_to :dish
  has_one :comment
  has_one :user
  
  has_attached_file :file, :default_url => "http://placehold.it/350x150"
  validates_attachment_content_type :file, :content_type => /\Aimage\/.*\Z/

  def as_json(options = { })
    
    options = options.merge(file_options())
    h = super(options)
    h[:url] = self.file.url
    h.delete("id")
    h.delete("file_file_name")
    h.delete("file_content_type")
    h.delete("file_file_size")
    h.delete("file_fingerprint")
    h.delete("file_updated_at")
    h
  end
  
  def file_options
    {
      :format => :table,
      :only => [:file_file_name, :file_updated_at  ]
    }
  end

  def file_url
      file.url
  end
    
end
