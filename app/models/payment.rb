class Payment < ActiveRecord::Base
  belongs_to :ordered
  
  def payment_approved 
    if gateway == MainYetting.GATEWAY_CIELO and
      (status.to_i == MainYetting.CIELO_STATUS_AUTORIZADA or status.to_i == MainYetting.CIELO_STATUS_CAPTURADA) then
      return true
    end
    return false
  end
end
