class Reserve < ActiveRecord::Base
  
  belongs_to :dish, inverse_of: :reserves
  belongs_to :user
  
  def total_value
    return delivery_price + ordered_value  
  end
  
  def as_json(options = { })
    options = options.merge(default_options())

    h = super(options)

    h
  end

private
  def default_options
    {
      :format => :table,
      :only => [:id, :quantity, :delivery_pick_up, :dish_id, :user_id, :dish_price, :delivery_price, :confirmed, :canceled, :attempts]
    }
  end
end
