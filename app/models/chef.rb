class Chef < ActiveRecord::Base

  has_many :kitchens  
  belongs_to :user
  has_one :bank_account_info
  
  accepts_nested_attributes_for :kitchens
  accepts_nested_attributes_for :bank_account_info, update_only: true

  def as_json(options = { })
    options = options.merge(default_options())
    if (not options[:kitchen].nil? and options[:kitchen]) then
      options = options.deep_merge(kitchen_options())
    end
    if (not options[:user].nil? and options[:user]) then
      options = options.deep_merge(user_options())
    end
    h = super(options)
    h
  end

private
  def default_options
    {
      :format => :table,
      :only => [:id, :about, :nr_rating, :avg_rating, :qt_not_accept_ordered, :qt_cancel_ordered]
    }
  end
  
  def kitchen_options
    {
      :include => {
        :kitchens => { 
          :only => [:id, :close, :verified],
          :include => {
            :address => {
              :only => [:id, :street, :number, :complement, :neighborhood, :zip_code, :city, :state, :country, :latitude, :longitude ]
            }        
          }
        }
      }
    }
  end
  
  def user_options
    {
      :include => {
        :user => {
          :only => [:id, :name, :email],
          :include => {
            :photo => {
              :only => [:id ],
              :methods => [:file_url]
            }
          }
        }
      }
    }
  end
  
end
