class SubscriptionMailer < ActionMailer::Base
  default from: "contato@bemychef.co"

  
  def subscription_mail(subscription, locale)
    @subscription = subscription 
    @locale = locale   
    mail(to: @subscription.email, subject: I18n.t(:subject_email_subscription), from: "BeMyChef <contato@bemychef.co>")
  end
  
end
