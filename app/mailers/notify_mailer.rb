class NotifyMailer < ActionMailer::Base
  default from: "noreply@bemychef.co"
  
  #notifica Chef o pedido do usuário
  def notify_ordered(chef, dish, ordered)
    @chef = chef
    @ordered = ordered
    @dish = dish
    @locale = @chef.locale
    mail(to: @chef.email, subject: I18n.t(:email_subject_notify_ordered, :locale => @locale))
  end
  
  #notifica o usuário que o chef aceitou o pedido
  def notify_accepted_ordered(chef, user, dish, ordered)
    @chef = chef
    @user = user
    @ordered = ordered
    @dish = dish
    @locale = @user.locale
    mail(to: @user.email, subject: I18n.t(:email_subject_accepted_ordered, :locale => @locale))
  end
  
  #notifica o usuário que o chef NÃO aceitou o pedido
  def notify_not_accepted_ordered(chef, user, dish, ordered)
    @chef = chef
    @user = user
    @ordered = ordered
    @dish = dish
    @locale = @user.locale
    mail(to: @user.email, subject: I18n.t(:email_subject_not_accepted_ordered, :locale => @locale))
  end
  
  #notifica o usuário que o chef cancelou o pedido
  def notify_ordered_cancel_by_chef(chef, user, dish, ordered)
    @chef = chef
    @user = user
    @ordered = ordered
    @dish = dish
    @locale = @user.locale
    mail(to: @user.email, subject: I18n.t(:email_subject_ordered_cancel_by_chef, :locale => @locale))
  end
  
  #notifica o chef que o usuário cancelou o pedido
  def notify_ordered_cancel_by_user(chef, user, dish, ordered)
    @chef = chef
    @user = user
    @ordered = ordered
    @dish = dish
    @locale = @chef.locale
    mail(to: @chef.email, subject: I18n.t(:email_subject_ordered_cancel_by_user, :locale => @locale))
  end
  
  #notifica o usuário que o prato/pedido está pronto
  def notify_ordered_ready(chef, user, dish, ordered)
    @chef = chef
    @user = user
    @ordered = ordered
    @dish = dish
    @locale = @user.locale
    mail(to: @chef.email, subject: I18n.t(:email_subject_ordered_ready, :locale => @locale))
  end
  
  #notifica o usuário que o pedido expirou - O chef não rejeitou e nem aceitou
  def notify_ordered_timeout_user(chef, user, dish, ordered)
    @chef = chef
    @user = user
    @ordered = ordered
    @dish = dish
    @locale = @user.locale
    mail(to: @user.email, subject: I18n.t(:email_subject_ordered_timeout, :locale => @locale))
  end

  #notifica o chef que o pedido expirou - O chef não rejeitou e nem aceitou
  def notify_ordered_timeout_chef(chef, user, dish, ordered)
    @chef = chef
    @user = user
    @ordered = ordered
    @dish = dish
    @locale = @chef.locale
    mail(to: @chef.email, subject: I18n.t(:email_subject_ordered_timeout, :locale => @locale))
  end

  ##COMENTÁRIOS
  #notifica o usuário que o comentário irá passar por aprovação
  def notify_comment_in_moderation(user, comment, dish) 
    @user = user
    @dish = dish
    @comment = comment
    @locale = @user.locale
    mail(to: @user.email, subject: I18n.t(:email_subject_comment_in_moderation, :locale => @locale))
  end
end
