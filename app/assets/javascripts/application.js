// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


jQuery.extend(jQuery.validator.messages, {
				required : "Campo obrigatório.",
				remote : "Valor inválido.",
				email : "Email inválido.",
				url : "URL inválida.",
				date : "Data inválida.",
				dateISO : "Data inválida (ISO).",
				number : "Número inválido.",
				digits : "Somente digitos são válidos.",
				creditcard : "Número de cartão inválido.",
				equalTo : "Entre o mesmo valor novamente.",
				accept : "Entre com um valor com uma extensão válida.",
				maxlength : jQuery.validator.format("Número máximo de caracteres {0}."),
				minlength : jQuery.validator.format("Deve ter no mínimo {0} caracteres."),
				rangelength : jQuery.validator.format("Entre com um valor entre {0} e {1}."),
				range : jQuery.validator.format("Entre com um valor entre {0} e {1}."),
				max : jQuery.validator.format("Entre com um valor menor que {0}."),
				min : jQuery.validator.format("Entre com um valor  maior que {0}.")
			});