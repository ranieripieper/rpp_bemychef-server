module SubscriptionHelper
  extend self
  
  
  def send_subscription_mail() 
    #ThreadUtility.with_connection do
      puts ">> Recupera subscriptions"
      subscriptions = Subscription.where(:email_sent => false).all
      if not subscriptions.nil? then
        puts ">> Total subscriptions #{subscriptions.length}"
        subscriptions.each do |subscription| 
          if ENV['RAILS_ENV'] == 'production'
            puts ">>> Enviando email subscription => #{subscription} - ENV=> #{ENV['RAILS_ENV']}"
            SubscriptionMailer.subscription_mail(subscription, I18n.locale).deliver!
          end
          subscription.email_sent = true
          subscription.save!
        end
      end
    #end
  end

end