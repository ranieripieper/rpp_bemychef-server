module UserHelper
  extend self
    
=begin
      unless Rails.application.config.consider_all_requests_local
        rescue_from Exception, with: lambda { |exception| render_error 500, exception }
        rescue_from ActionController::RoutingError, ActionController::UnknownController, ::AbstractController::ActionNotFound, ActiveRecord::RecordNotFound, with: lambda { |exception| render_error 404, exception }
      end
=end      
      def authenticate_user_from_token!
        user_token = "#{request.headers['User-Token']}"
        
        puts ">>>>> authenticate_user_from_token! #{user_token}"
    
        if user_token.nil? or user_token.empty? then
          user_token = params['User-Token'].presence
        end
        
        #user_token = params[:user_token].presence
        if user_token then
          #pos_id = user_token.index(":")
          #id = user_token[0..pos_id-1]
          #token = user_token[pos_id+1..user_token.size]
          user = User.find_by_authentication_token(user_token)
        end
       
     
        if user
          # Notice we are passing store false, so the user is not
          # actually stored in the session and a token is needed
          # for every request. If you want the token to work as a
          # sign in token, you can simply remove store: false.
          sign_in user, store: false
        end
        
        log_additional_data()
      end

      
private
      def render_error(status, exception)
        puts exception.message
        ExceptionNotifier.notify_exception(exception, env: request.env, data: {current_user: current_user.nil? ? "" : current_user.id})
        respond_to do |format|
           format.json { render :json => {:success => false, :status => status,
                                          :info => "Ocorreu um erro"} }
        end
      end
      
protected
      def log_additional_data
        request.env["exception_notifier.exception_data"] = {
          :current_user => current_user.nil? ? "" : current_user.id
        }
      end  
      
      def set_csrf_header
         response.headers['X-CSRF-Token'] = form_authenticity_token
      end
end