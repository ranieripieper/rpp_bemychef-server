module PaymentHelper
  extend self
  
  
  def process_payment(user_id, ordered_id, dish_id, card_holder_name, card_number, card_cvv, card_exp_year, card_exp_month, card_flag, value) 
    transaction = Cielo::Transaction.new
    campo_livre = "u:#{user_id} _ ord:#{ordered_id} "
    valor = value * 100
    
    transaction_parameters = {
      numero: "#{ordered_id}",
      valor: "#{valor.to_i}",
      moeda: MainYetting.GATEWAY_CIELO_MOEDA_REAIS,
      bandeira: card_flag.downcase,
      :'campo-livre' => campo_livre,
      autorizar: '3',
      capturar: "false",
      :'url-retorno' => 'http://www.bemychef.co',
      cartao_numero: card_number,
      cartao_validade: "#{card_exp_year}#{card_exp_month}",
      cartao_seguranca: card_cvv,
      cartao_portador: card_holder_name
    }

    res = transaction.create!(transaction_parameters, :store)

    transaction_id = nil
    status = nil

    if not res.nil? and res[:erro].nil? then
      transacao = res[:transacao]
      if not transacao.nil? then
        transaction_id = transacao[:tid]
        status = transacao[:status]

        Rails.logger.payment.info("User: #{user_id} - Dish: #{dish_id} - Ordered #{ordered_id} - transaction_id #{transaction_id} - status: #{status}")
        
        payment = Payment.new
        payment.ordered_id = ordered_id
        payment.value = value
        payment.transaction_id = transaction_id
        payment.status = status
        payment.gateway = MainYetting.GATEWAY_CIELO
        payment.card_flag = card_flag.downcase
        payment.last_card_nr = card_number[card_number.length-4...card_number.length]
        payment.save
        return payment
        
      end
    else
      Rails.logger.payment.error("Erro: #{res[:erro][:codigo]} - #{res[:erro][:mensagem]} - User: #{user_id} - Dish: #{dish_id} - Ordered #{ordered_id} - transaction_id #{transaction_id} - status: #{status}")
    end
    
    return false
  end
  
  def capture_payment(ordered_id) 

  end

  
  def cancel_cielo_payment(transaction_id, status = nil, message = nil, ordered_id = nil)
    if not transaction_id.nil? then
      ActiveRecord::Base.transaction do
        #cancelamento de pagamento
        cancel_payment = CancelPayment.new
        cancel_payment.ordered_id = ordered_id
        cancel_payment.transaction_id = transaction_id 
        cancel_payment.status = status
        cancel_payment.gateway = MainYetting.GATEWAY_CIELO
        cancel_payment.message = message
        cancel_payment.save  
      end
    end 
  end
  
  

  def cancel_payments
    
    page = 1
    cancel_payments = CancelPayment.where(:status => 4).paginate(:page => page, :per_page => MainYetting.DEFAULT_REG_PER_PAGE).all
    if not cancel_payments.nil? and not cancel_payments.empty? then
      puts "Cancelando pagamentos "
    end
    
    while not cancel_payments.nil? and not cancel_payments.empty? do
      cancel_payments.each do |cancel_payment|
        cancel_payment(cancel_payment)
      end
      
      cancel_payments = CancelPayment.where(:status => 4).paginate(:page => page, :per_page => MainYetting.DEFAULT_REG_PER_PAGE).all
    end
    
  end
  
  def cancel_payment(cancel_payment)
    transaction = Cielo::Transaction.new
    res = transaction.cancel!(cancel_payment.transaction_id)
  
    if not res.nil? and res[:erro].nil? then
      transacao = res[:transacao]
      
      if not transacao.nil? then
        cancel_payment.status = transacao[:status]
        cancel_payment.save
        
        if cancel_payment.status.to_i == MainYetting.CIELO_STATUS_CANCELADA
          Rails.logger.cancel_payment.info("Pagamento cancelado #{cancel_payment.id}")
        end
        
      end
    end
  end
end