# -*- encoding : utf-8 -*-
module BeMyChef
  class AddressesService < BaseService
    
    def initialize(params = nil, options={})
      @params             = params
      @options            = options
    end
    
    
    def get_address_by_cep(cep) 
      finder = Correios::CEP::AddressFinder.new
      address = finder.get(cep) rescue nil
      address
    end
    
    def get_addresses_delivered(user_id) 
      addresses =  Address.joins(:ordereds)
                          .where("ordereds.user_id = ? and delivery_pick_up != ?", user_id, MainYetting.ORDERED_PICK_UP)
                          .distinct
    end
    
  end
end