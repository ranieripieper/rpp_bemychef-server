# -*- encoding : utf-8 -*-
module BeMyChef
  class OrderedsService < BaseService   
    
    def initialize
    end
    
    def has_ordered_in_process(user_id, dish_id) 
      ordereds = Ordered.where("user_id = ? AND dish_id = ? AND last_ordered_status_id in (?) ", user_id, dish_id, 
      [MainYetting.ORDERED_STATUS_ORDERED, MainYetting.ORDERED_STATUS_ACCEPTED]).count
      if ordereds > 0 then
        return true
      end
      return false
    end
    
    def has_ordered(user_id, dish_id, status) 
      ordereds = Ordered.where("user_id = ? AND dish_id = ? AND last_ordered_status_id = ? ", user_id, dish_id, status).count
      if ordereds > 0 then
        return true
      end
      return false
    end
    
    def verify_and_cancel_reserves(user_id, dish) 
      reserves = Reserve.where("user_id = ? AND dish_id = ? AND canceled = ? AND confirmed = ? ", user_id, dish.id, false, false).all
      if not reserves.nil? and not reserves.empty? then
        reserves.each do |reserve|
          reserve.canceled = true
          reserve.save
        end
        #faz update da quantidade reservada do prato
        dish.quantity_reserved = Reserve.where("user_id = ? AND dish_id = ? AND canceled = ?  AND confirmed = ? ", user_id, dish.id, false, false).count
        dish.save
      end
    end
    
    def reserve_timeout(reserve_id = nil)
      
      if reserve_id.nil? then
        reserves = Reserve.joins(:dish).includes(:dish).lock(true)
                          .where("reserves.canceled = ? AND reserves.confirmed = ? AND reserves.updated_at + INTERVAL ? SECOND < NOW() ", 
                                  false, false, MainYetting.TIMEOUT_CONFIRM_RESERVE)
    
        reserves.each do |reserve|
         process_reserve_timeout(reserve)
        end
      else
        reserve = Reserve.joins(:dish).includes(:dish).lock(true)
                          .where("reserves.id = ? AND reserves.canceled = ? AND reserves.confirmed = ? AND reserves.updated_at + INTERVAL ? SECOND < NOW()",
                                  ordered_id, false, false, MainYetting.TIMEOUT_CONFIRM_RESERVE).first
        process_reserve_timeout(ordered)
      end 
    end
    
    def ordered_timeout(ordered_id = nil)
      
      if ordered_id.nil? then
        ordereds = Ordered.lock(true)
                          .where("last_ordered_status_id = ? AND created_at + INTERVAL #{MainYetting.TIMEOUT_ACCEPT_ORDERED} SECOND < NOW() ", 
                                  MainYetting.ORDERED_STATUS_ORDERED)
    
        ordereds.each do |ordered|
         process_ordered_timeout(ordered)
        end
      else
        ordered = Ordered.lock(true)
                          .where("id = ? and last_ordered_status_id = ? AND created_at + INTERVAL #{MainYetting.TIMEOUT_ACCEPT_ORDERED} SECOND < NOW()",
                                  ordered_id, MainYetting.ORDERED_STATUS_ORDERED).first
        process_ordered_timeout(ordered)
      end 
    end
   
    def create_ordered(user, dish, reserve, address) 
      ordered = Ordered.new
      ordered.user_id = reserve.user_id
      ordered.dish_id = reserve.dish_id
      ordered.dish_price = dish.price
      ordered.quantity = reserve.quantity
      ordered.ordered_value = reserve.ordered_value
      ordered.delivery_price = reserve.delivery_price
      ordered.address = address
      ordered.last_ordered_status_id = MainYetting.ORDERED_STATUS_ORDERED
  
      ordered.save
      
      update_ordered_status(ordered, MainYetting.ORDERED_STATUS_ORDERED)
      
      return ordered
    end
     
    def create_ordered_old(user, dish, quantity, ps, delivery_pick_up) 
      ordered = Ordered.new
      ordered.user_id = user.id
      ordered.dish_id = dish.id
      ordered.quantity = quantity
      ordered.ps = ps
      ordered.delivery_pick_up = delivery_pick_up
      ordered.address_id = get_address_id(user, dish, delivery_pick_up)
      ordered.last_ordered_status_id = MainYetting.ORDERED_STATUS_ORDERED
      ordered.save
      
      update_ordered_status(ordered, MainYetting.ORDERED_STATUS_ORDERED)
      
      return ordered
    end
    
    
    
    def update_ordered_status(ordered, status_id) 
      ordered_status = OrderedStatus.new
      ordered_status.ordered_id = ordered.id
      ordered_status.status_id = status_id
      ordered_status.save
      ordered.last_ordered_status_id = status_id
      ordered.save
      ordered_status
    end
    
    #calcula o preço do pedido
    def get_ordered_price(dish, quantity, delivery_pick_up)
       if MainYetting.ORDERED_CHEF_DELIVERY == delivery_pick_up or 
         MainYetting.ORDERED_DELIVERY == delivery_pick_up then
        return dish.price * quantity + MainYetting.DELIVERY_PRICE
      elsif MainYetting.ORDERED_PICK_UP == delivery_pick_up then
        return dish.price * quantity
      end
    end
    
  private
    
    def process_ordered_timeout(ordered) 
      if not ordered.nil? then
        ActiveRecord::Base.transaction do
          update_ordered_status(ordered, MainYetting.ORDERED_STATUS_ORDERED_TIMEOUT)
          #faz update do chef
          service = BeMyChef::ChefService.new
          service.update_ordered_timeout(ordered)
          BeMyChef::NotificationService.new.notify_ordered_timeout(ordered)
        end
      end
    end
    
    
    def process_reserve_timeout(reserve) 
      if not reserve.nil? then
        
        ActiveRecord::Base.transaction do
          reserve.canceled = true
          reserve.save
          
          #faz update da quantidade reservada do prato
          reserve.dish.quantity_reserved = Reserve.where("dish_id = ? AND canceled = ?  AND confirmed = ? ", reserve.dish.id, false, false).count
          reserve.dish.save
        end
      end
    end
    
  end
end