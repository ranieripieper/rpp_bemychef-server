# -*- encoding : utf-8 -*-
module BeMyChef
  class ChefService < BaseService   
    
    def initialize
    end
    
    def get_chef_by_id(chef_id) 
      Chef.joins([{:kitchens =>  :address}, :user])
                  .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
                  .includes([{:kitchens =>  :address}, :user => :photo])
                  .find(chef_id)
    end
    
    def get_chef_by_user_id(user_id)
      
       
      chef = Chef.joins([{:kitchens =>  :address}, :user => :photo])
                  .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
                  .includes([{:kitchens =>  :address}, :user => :photo])
                  .where("`users`.`id` = ? ", user_id).first
      
      raise ActiveRecord::RecordNotFound if chef.nil?
      
      chef
    end
    
    def get_value_paid(user_id)
      Ordered.joins([{:dish => [:kitchen => [:chef]]}])
                      .where("`chefs`.`user_id` = ? and `last_ordered_status_id` = ? and `paid_for_chef` = ? ", user_id, MainYetting.LAST_STATUS_ORDERED, true)
                      .sum(:price)
      
    end

    def get_value_not_paid(user_id)
      Ordered.joins([{:dish => [:kitchen => [:chef]]}])
                          .where("`chefs`.`user_id` = ? and `last_ordered_status_id` = ? and `paid_for_chef` = ? ", user_id, MainYetting.LAST_STATUS_ORDERED, false)
                          .sum(:price)
    end 
    
  def close_kitchen(kitchen) 
    kitchen.dishes.update_all(:available => false, :quantity => 0)
    kitchen.dt_close = Time.now
    kitchen.close = true
    kitchen.save
  end
  
  def update_ordered_timeout(ordered) 
      where_clause = ActiveRecord::Base.send(
                                  :sanitize_sql_array, [" ordereds.dish_id = dishes.id
                                                          and dishes.kitchen_id = kitchens.id
                                                          and ordereds.id = ?", ordered.id])
      chef_id = ActiveRecord::Base.send(:sanitize_sql_array, ["id = ?", chef_id])
      sql_update = "  update chefs set qt_timeout_ordered = qt_timeout_ordered+1
                      where id = (select chef_id from 
                      ordereds, dishes, kitchens
                      where #{where_clause})"
    
      results_update = ActiveRecord::Base.connection.execute(sql_update)
  end
  
  def update_rating_by_dish_id(dish_id) 
    #faz update do rating do chef
    query_chef_id = ActiveRecord::Base.send(:sanitize_sql_array, ["dishes.id = ?", dish_id])
    results = ActiveRecord::Base.connection.execute("select kitchens.chef_id FROM dishes INNER JOIN kitchens ON kitchens.id = dishes.kitchen_id where #{query_chef_id}")

    chef_id = -1
    if not results.nil? and results.size > 0 then
      results.each do |row|
        chef_id = row[0]
        break
      end                              
    end
    
    update_rating(chef_id)
  end
  
  def update_rating(chef_id)
    if chef_id > 0 then
      kitchens_chef_id = ActiveRecord::Base.send(:sanitize_sql_array, ["kitchens.chef_id = ?", chef_id])
      chef_id = ActiveRecord::Base.send(:sanitize_sql_array, ["id = ?", chef_id])
      sql_update = "  update chefs 
                      set nr_rating = 
                        (select count(*) from comments 
                        where dish_id in 
                          (select dishes.id 
                          FROM `dishes` INNER JOIN `kitchens` ON `kitchens`.`id` = `dishes`.`kitchen_id` WHERE (#{kitchens_chef_id}))
                    
                        ),
                        avg_rating = 
                        (select sum(rating)/count(*) from comments 
                        where dish_id in 
                          (select dishes.id 
                          FROM `dishes` INNER JOIN `kitchens` ON `kitchens`.`id` = `dishes`.`kitchen_id` WHERE (#{kitchens_chef_id}))
                    
                        )
                    where #{chef_id}"
    
      results_update = ActiveRecord::Base.connection.execute(sql_update)
      
    end
  end
  end
end