# -*- encoding : utf-8 -*-
module BeMyChef
  class ProcessPaymentPagarMeService < BaseService

    def initialize
    end
    
    def initialize(user, params, options={})
      @user               = user
      @params             = params
      @options            = options
    end
    
    def status
      @status
    end

    def json_response
      @json
    end
    
    def process
#      begin
        reserve_id = @params[:reserve_id].to_i
        card_hash = @params[:card_hash]
    
        transaction_id = nil
        payment_status = nil

        if reserve_id.nil? or card_hash.nil? then
           @status = 200
           @json = { :success => false,
                        :info => I18n.t(:msg_invalid_parameters)}
          return 
        end
          
        ActiveRecord::Base.transaction do

          reserve = Reserve.joins(:dish).includes(:dish).lock(true).where(:id => reserve_id, :user_id => @user.id).first

          if reserve.nil? or reserve.canceled then
             @status = 200
             @json = { :success => false,
                              :refresh_dish => true,
                              :info => I18n.t(:msg_invalid_reserve),
                              :reserve => reserve.as_json() }
            return
          end

          qt = reserve.quantity
          delivery_pick_up = reserve.delivery_pick_up
          address = nil
          dish = reserve.dish
          
          #verifica o endereço 
          if reserve.delivery_pick_up != MainYetting.ORDERED_PICK_UP then 
            if @params[:address].nil? then
               @status = 200
               @json = {  :success => false,
                          :address_required => true,
                          :info => I18n.t(:msg_address_delivery_required)}
              return
            end        
            address = Address.new(address_params)
            if not address.nil? and not address.zip_code.nil? then
              address_correios = Correios::CEP::AddressFinder.get(address.zip_code)
              if address_correios.nil? then
                 @status = 200
                 @json = { :success => false,
                                  :address_required => true,
                                  :info => I18n.t(:msg_address_delivery_required)}
                return
              end
              if address.street.nil? then
                address.street = address_correios[:address]
              end
              address.neighborhood = address_correios[:neighborhood]
              address.city = address_correios[:city]
              address.state = address_correios[:state]
              address.country = "Brasil"
              address.save
            end
          else
            address = dish.kitchen.address
          end
    
          #verifica se já tem um pedido aguardando confirmação do chef
          if OrderedHelper::has_ordered(@user.id, reserve.dish_id, MainYetting.ORDERED_STATUS_ORDERED) then
             @status = 200
             @json = { :success => false,
                      :info => I18n.t(:msg_ordered_in_process)}
            return
          end

          #cria o pedido
          ordered = OrderedHelper::create_ordered(@user, dish, reserve, address)
          #realiza o pagamento - não realiza a captura
          puts "process pgto"
          payment = process_payment(@user.id, ordered.id, reserve.dish_id, reserve.total_value, card_hash)
          puts "...."
          transaction_id = payment.transaction_id
          status = payment.status
          
          if not payment.payment_approved then
             @status = 200
             @json = { :success => false,
                      :info => I18n.t(:msg_payment_not_approved),
                      :dish => dish.as_json(:only_dish => true) }
            
            raise ActiveRecord::Rollback
            
          else
            #confirma a reserva e diminui o nr. de pratos disponíveis
            reserve.canceled = false
            reserve.confirmed = true
            reserve.save
            
            dish.quantity_sale = dish.quantity_sale + reserve.quantity
            dish.quantity_reserved = dish.quantity_reserved - reserve.quantity
            dish.save

            #envia notificação para o chef
            BeMyChef::NotificationService.new.notify_ordered(dish, ordered)
            
            #schedule task para cancelar o pedido automaticamente
            scheduler = Rufus::Scheduler.new
            scheduler.in "#{MainYetting.TIMEOUT_ACCEPT_ORDERED}s" do
              puts "**** Cancelando pedido automaticamente"
              OrderedHelper::ordered_timeout(ordered.id)
            end
          end          
          
          render :status => 200,
                 :json => { :success => true,
                            :info => I18n.t(:msg_ordered_send),
                            :dish => dish.as_json(:only_dish => true),
                            :ordered => ordered}
        end # fim da transação
=begin
      rescue  => e
        puts e
        
        if not reserve.nil? then
          reserve.attempts = reserve.attempts + 1
          if MainYetting.ORDERED_ATTEMPTS <= reserve.attempts then
            reserve.canceled = true
          end
          reserve.save
        end        
        
        Rails.logger.payment.error("Erro ao processar pagamento. User: #{get_user().id} - transaction_id: #{transaction_id} - Erro: => #{e.message}")
      
        PaymentHelper::cancel_cielo_payment(transaction_id, status, e.message, nil)
        
        render :status => 200,
                   :json => { :success => false,
                        :info => I18n.t(:msg_internal_error)}
      end 
=end
    end

private
    def process_payment(user_id, ordered_id, dish_id, total_value, card_hash)

      value = total_value * 100
    
      PagarMe.api_key = "ak_test_Bdb8iQ6ZqdRYdTifOLiRsZxivmiBqT";
      
      transaction = PagarMe::Transaction.new({
          :amount => value,
          :card_hash => "{card_hash}",
          :capture => false,
          :soft_descriptor => MainYetting.PAGAR_ME_SOFT_DESCRIPTION,
          :metadata => {
              :user_id => user_id,
              :ordered_id => ordered_id
          }
      })
      
      transaction.charge

      transaction_id = transaction.id
      status = transaction.status
  
      if not transaction_id.nil? and status.nil? then
  
        Rails.logger.payment.info("User: #{user_id} - Dish: #{dish_id} - Ordered #{ordered_id} - transaction_id #{transaction_id} - status: #{status}")
        
        payment = Payment.new
        payment.ordered_id = ordered_id
        payment.value = value
        payment.transaction_id = transaction_id
        payment.status = status
        payment.gateway = MainYetting.GATEWAY_PAGAR_ME
        #payment.card_flag = card_flag.downcase
        #payment.last_card_nr = card_number[card_number.length-4...card_number.length]
        payment.save
        return payment

      else
        Rails.logger.payment.error("Erro: #{res[:erro][:codigo]} - #{res[:erro][:mensagem]} - User: #{user_id} - Dish: #{dish_id} - Ordered #{ordered_id} - transaction_id #{transaction_id} - status: #{status}")
      end
      
      return false
    end

  end
end