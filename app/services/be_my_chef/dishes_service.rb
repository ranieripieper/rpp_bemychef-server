# -*- encoding : utf-8 -*-
module BeMyChef
  class DishesService < BaseService   
    
    def initialize
    end
    
    def get_chef_by_id(chef_id) 
      Chef.joins([{:kitchens =>  :address}, :user])
                  .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `users`.`photo_id` ')
                  .includes([{:kitchens =>  :address}, :user => :photo])
                  .find(chef_id)
    end
    
  end
end