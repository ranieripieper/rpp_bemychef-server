# -*- encoding : utf-8 -*-
module BeMyChef
  class RatingService < BaseService   
    
    def initialize
    end
    
    def update_rating_by_dish_id(dish_id) 
      #faz update do rating do chef
      query_chef_id = ActiveRecord::Base.send(:sanitize_sql_array, ["dishes.id = ?", dish_id])
      results = ActiveRecord::Base.connection.execute("select kitchens.chef_id FROM dishes INNER JOIN kitchens ON kitchens.id = dishes.kitchen_id where #{query_chef_id}")
  
      chef_id = -1
      if not results.nil? and results.size > 0 then
        results.each do |row|
          chef_id = row[0]
          break
        end                              
      end
      
      update_rating(chef_id)
    end
    
    def update_rating(chef_id)
      if chef_id > 0 then
        kitchens_chef_id = ActiveRecord::Base.send(:sanitize_sql_array, ["kitchens.chef_id = ?", chef_id])
        chef_id = ActiveRecord::Base.send(:sanitize_sql_array, ["id = ?", chef_id])
        sql_update = "  update chefs 
                        set nr_rating = 
                          (select count(*) from comments 
                          where dish_id in 
                            (select dishes.id 
                            FROM `dishes` INNER JOIN `kitchens` ON `kitchens`.`id` = `dishes`.`kitchen_id` WHERE (#{kitchens_chef_id}))
                      
                          ),
                          avg_rating = 
                          (select sum(rating)/count(*) from comments 
                          where dish_id in 
                            (select dishes.id 
                            FROM `dishes` INNER JOIN `kitchens` ON `kitchens`.`id` = `dishes`.`kitchen_id` WHERE (#{kitchens_chef_id}))
                      
                          )
                      where #{chef_id}"
      
        results_update = ActiveRecord::Base.connection.execute(sql_update)
        
      end
    end
  end
end