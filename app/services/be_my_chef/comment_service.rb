# -*- encoding : utf-8 -*-
module BeMyChef
  class CommentService < BaseService   
    
    def initialize
    end
    
    def create(user, ordered_id, dish_id, params) 
      #verifica se o existe o pedido
      count_ordered = Ordered.where("dish_id = ? and user_id = ? and last_ordered_status_id = ? and id = ?", dish_id, user.id, MainYetting.ORDERED_STATUS_FOR_COMMENT, ordered_id).count
      raise ActiveRecord::RecordNotFound if count_ordered <= 0
      #verifica se usuário já fez algum comentário para o pedido/prato - Só pode realizar um comentário por prato/pedido
      comment = Comment.where("dish_id = ? and user_id = ? and ordered_id = ?", dish_id, user.id, ordered_id).first
      if comment.nil? then
        ActiveRecord::Base.transaction do
          comment = Comment.new(comment_params(params))
          comment.user_id = user.id
          comment.dish_id = dish_id
          comment.ordered_id = ordered_id
          comment.approved = !MainYetting.APPROVE_COMMENT
          comment.save
          
          BeMyChef::RatingService.new(params).update_rating_by_dish_id(dish_id)
    
          if MainYetting.APPROVE_COMMENT then
            #envia notificação para o usuário que o comentário irá passar por moderação
            BeMyChef::NotificationService.new.notify_comment_in_moderation(user, comment, dish_id, ordered_id)
          end
          
          comment
        end
      else
        raise ActionController::RoutingError.new(I18n.t("msg_comment_already_exist"))
      end
    end

    def update(user, comment_id, params)
      ActiveRecord::Base.transaction do
        comment = Comment.where(:id => comment_id, :user_id => user.id).first
        raise ActiveRecord::RecordNotFound if comment.nil?
        params[:comment][:approved] = !MainYetting.APPROVE_COMMENT
        if comment.update_attributes comment_params(params) then
          BeMyChef::RatingService.new(params).update_rating_by_dish_id(comment.dish_id)
          
          if MainYetting.APPROVE_COMMENT then
            #envia notificação para o usuário que o comentário irá passar por moderação
            BeMyChef::NotificationService.new.notify_comment_in_moderation(user, comment, comment.dish_id, comment.ordered_id)
          end
              
          comment
        else
          add_errors(comment.errors.full_messages)
        end
      end
    end
  
    def get_comments(dish_id, page = 1, per_page = MainYetting.DEFAULT_REG_PER_PAGE)
      if page.nil? or page.empty? then
        page = 1
      end
      if per_page.nil? or per_page.empty? then
        per_page = MainYetting.DEFAULT_REG_PER_PAGE
      end
      
      return Comment
                        .joins(:user)
                        .joins('LEFT OUTER JOIN `photos` ON `photos`.`id` = `comments`.`photo_id` ')
                        .joins('LEFT OUTER JOIN `photos` as photos_user ON `photos_user`.`id` = `users`.`photo_id` ')
                        .includes(:user)           
                        .where(:dish_id => dish_id.to_i)
                        .order("comments.created_at desc")
                        .paginate(:page => page.to_i, :per_page => per_page.to_i)  
    end
    
    private
      def comment_params(params)
        params.require(:comment).permit(:approved, :comment, :rating, photo_attributes: [:file, :file_content_type])
      end 
  
  end
end