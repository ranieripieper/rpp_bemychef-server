# -*- encoding : utf-8 -*-
module BeMyChef
  class BaseService

    attr_reader :errors
    def initialize(params = nil, options = {})
      @params             = params
      @options            = options
    end

    def valid?
      validates if @validated.blank?
      @validated = true
      return @errors.blank?
    end

    def success?
      @success == true
    end

    private

    def add_error(error)
      @errors ||= []
      @errors << error
    end

    def add_errors(new_errors)
      @errors ||= []
      @errors |= new_errors
    end

  end
end

