# -*- encoding : utf-8 -*-
module BeMyChef
  class NotificationService < BaseService   
    
    @@CHEF_CHANNELS = [MainYetting.PARSE_CHANNEL_GLOBAL, MainYetting.PARSE_CHANNEL_CHEFS]
    @@USER_CHANNELS = [MainYetting.PARSE_CHANNEL_GLOBAL, MainYetting.PARSE_CHANNEL_ONLY_USERS]
    
    def initialize
    end
    
    #salva o token para o usuário
    def save_push_notification(user, platform, device_token) 
      if not user.nil? and not platform.nil? and not device_token.nil? and not platform.blank? and not device_token.blank? then
        #salva no banco de dados
        user_push_token = UserPushToken.find_or_create_by(user_id: user.id, token: device_token, platform: platform) do |tmp|
          tmp.user_id = user.id
          tmp.notify = true
        end
        UserPushToken.delete_all(["user_id != ? AND token = ? AND platform = ?", user.id, device_token, platform])
        
        #registra no parse
        if not user.chef.nil? then
          register_chef(user, platform, device_token)
        else
          register_user(user, platform, device_token)
        end
      end

    end
    
    #registra chef
    def register_chef(user, platform, device_token) 
      register(user, platform, device_token, @@CHEF_CHANNELS.dup)
    end
    
    #registra chef
    def register_user(platform, device_token) 
      register(user, platform, device_token, @@USER_CHANNELS.dup)
    end
    
    ###################################  
    ##### NOTIFICAÇÕES ################
    ###################################
      
    #notifica Chef o pedido do usuário
    def notify_ordered(dish, ordered) 
      user = User.joins(:chef => [:kitchens => [:dishes]]).where("dishes.id = ?", dish.id).first 
      create_notification_chef(user.chef, ordered, "text_chef_notify_ordered", MainYetting.NOTIFY_TYPE_ORDERED_CHEF)
      Thread.new do
        NotifyMailer.notify_ordered(chef, dish, ordered).deliver!
      end
      send_push_notification(chef, "text_chef_notify_ordered", "{EN}text_chef_notify_ordered", MainYetting.NOTIFY_TYPE_ORDERED_CHEF)
    end
    
    #notifica o usuário que o chef aceitou o pedido
    def notify_accepted_ordered(chef, ordered) 
      user = User.where("id = ?", ordered.user_id).first
      dish = Dish.find(ordered.dish_id)
      
      create_notification_user(user, ordered, "text_accepted_ordered", MainYetting.NOTIFY_TYPE_ORDERED_ACCEPT)
      
      Thread.new do
        NotifyMailer.notify_accepted_ordered(chef, user, dish, ordered).deliver!
      end
      send_push_notification(user, "text_accepted_ordered", "{EN}text_accepted_ordered", MainYetting.NOTIFY_TYPE_ORDERED_ACCEPT)
    end
    
    #notifica o usuário que o chef NÃO aceitou o pedido
    def notify_not_accepted_ordered(chef, ordered) 
      user = User.where("id = ?", ordered.user_id).first
      dish = Dish.find(ordered.dish_id)
      create_notification_user(user, ordered, "text_not_accepted_ordered", MainYetting.NOTIFY_TYPE_ORDERED_NOT_ACCEPT)
      Thread.new do
        NotifyMailer.notify_not_accepted_ordered(chef, user, dish, ordered).deliver!
      end
      send_push_notification(user, "text_not_accepted_ordered", "{EN}text_not_accepted_ordered", MainYetting.NOTIFY_TYPE_ORDERED_NOT_ACCEPT)
    end
    
    #notifica o usuário e o chef que o pedido expirou
    def notify_ordered_timeout(ordered) 
      user = ordered.user
      dish = ordered.dish
      chef = User.joins(:chef => [:kitchens => [:dishes]]).where("dishes.id = ?", dish.id).first
      create_notification_user(user, ordered, "text_ordered_timeout", MainYetting.NOTIFY_TYPE_ORDERED_TIMEOUT)
      create_notification_chef(chef.chef, ordered, "text_ordered_timeout", MainYetting.NOTIFY_TYPE_ORDERED_TIMEOUT)
      Thread.new do
        NotifyMailer.notify_ordered_timeout_user(chef, user, dish, ordered).deliver!
        NotifyMailer.notify_ordered_timeout_chef(chef, user, dish, ordered).deliver!
      end
      send_push_notification(user, "text_ordered_timeout", "{EN}text_ordered_timeout", MainYetting.NOTIFY_TYPE_ORDERED_TIMEOUT)
      send_push_notification(chef, "text_ordered_timeout", "{EN}text_ordered_timeout", MainYetting.NOTIFY_TYPE_ORDERED_TIMEOUT)
    end
    
    #notifica o usuário que o chef cancelou o pedido
    def notify_ordered_cancel_by_chef(chef, ordered) 
      user = User.where("id = ?", ordered.user_id).first
      dish = Dish.find(ordered.dish_id)
      create_notification_user(user, ordered, "text_ordered_cancel_by_chef", MainYetting.NOTIFY_TYPE_ORDERED_CANCEL_BY_CHEF)
      Thread.new do
        NotifyMailer.notify_ordered_cancel_by_chef(chef, user, dish, ordered).deliver!
      end
      send_push_notification(user, "text_ordered_cancel_by_chef", "{EN}text_ordered_cancel_by_chef", MainYetting.NOTIFY_TYPE_ORDERED_CANCEL_BY_CHEF)
    end
  
    #notifica o chef que o usuário cancelou o pedido
    def notify_ordered_cancel_by_user(user, ordered) 
      dish = Dish.find(ordered.dish_id)
      chef = User.joins(:chef => [:kitchens => [:dishes]]).where("dishes.id = ?", dish.id).first
      create_notification_chef(chef.chef, ordered, "text_ordered_cancel_by_user", MainYetting.NOTIFY_TYPE_ORDERED_CANCEL_BY_USER)
      Thread.new do      
        NotifyMailer.notify_ordered_cancel_by_user(chef, user, dish, ordered).deliver!
      end
      send_push_notification(chef, "text_ordered_cancel_by_user", "{EN}text_ordered_cancel_by_user", MainYetting.NOTIFY_TYPE_ORDERED_CANCEL_BY_USER)
    end
  
    #notifica o usuário que o prato/pedido está pronto
    def notify_ordered_ready(chef, ordered) 
      user = User.where("id = ?", ordered.user_id).first
      dish = Dish.find(ordered.dish_id)
      create_notification_user(user, ordered, "text_ordered_ready", MainYetting.NOTIFY_TYPE_ORDERED_READY)
      Thread.new do
        NotifyMailer.notify_ordered_ready(chef, user, dish, ordered).deliver!
      end
      send_push_notification(user, "text_ordered_ready", "{EN}text_ordered_ready", MainYetting.NOTIFY_TYPE_ORDERED_READY)
    end
    
    ##COMENTÁRIOS
    
    #notifica o usuário que o comentário irá passar por aprovação
    def notify_comment_in_moderation(user, comment, dish_id, ordered_id) 
      dish = Dish.find(dish_id)
      ordered = Ordered.find(ordered_id)
      create_notification_user(user, ordered, "text_comment_in_moderation", MainYetting.NOTIFY_TYPE_COMMENT_IN_MODERATION, comment)
      Thread.new do      
        NotifyMailer.notify_comment_in_moderation(user, comment, dish).deliver!
      end
      send_push_notification(user, "text_comment_in_moderation", "{EN}text_comment_in_moderation", MainYetting.NOTIFY_TYPE_COMMENT_IN_MODERATION)
    end
    
    def create_notification_chef(chef, ordered, i18n_text, type) 
      notification = ChefNotification.new 
      notification.chef = chef
      notification.ordered = ordered
      notification.text = I18n.t(i18n_text)
      notification.notification_type = type
      notification.save
    end
    
    def create_notification_user(user, ordered, i18n_text, type, comment = nil) 
      notification = UserNotification.new 
      notification.user = user
      notification.ordered = ordered
      notification.comment = comment
      notification.text = I18n.t(i18n_text)
      notification.notification_type = type
      notification.save
    end
    
    private
    
    def send_push_notification(user, msg, msg_en, type)
      if not user.nil? and not user.user_push_tokens.nil? then
        client = Parse.create(
          :application_id => MainYetting.PARSE_APP_ID, 
          :api_key        => MainYetting.PARSE_API_KEY, 
          :quiet      => false)
      
        data = { :alert => msg, :alert_en => msg, :type => type }
        
        #envia o push para o canal do usuário
        push = client.push(data, user.get_parse_channel)
        push.save
    
      end
    end
    
    def register(user, platform, device_token, channels)
      if not platform.nil? and not device_token.nil? and not platform.blank? and not device_token.blank? then
        client = Parse.create :application_id => MainYetting.PARSE_APP_ID,
             :api_key        =>  MainYetting.PARSE_API_KEY,
             :quiet          => false  # optional, defaults to false
             
        channels << user.get_parse_channel
        installation = client.installation.tap do |i|
          i.device_token = device_token
          i.device_type = platform
          if platform == MainYetting.PARSE_PLATFORM_ANDROID then
            i.push_type = "gcm"
          end
          i.app_name = "BeMyChef"
          i.channels = channels
        end
        installation.save
      end
      false
    end
  end
end