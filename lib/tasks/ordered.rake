require "#{Rails.root}/app/helpers/ordered_helper"
include OrderedHelper

namespace :ordered do
  desc "Jobs referentes ao pedido"
  
  
  task :ordered_timeout_task, [:ordered_id]  => :environment  do |t, args|

    ordered_timeout(args.ordered_id)
    
  end

end
