require "#{Rails.root}/app/helpers/subscription_helper"
require "#{Rails.root}/lib/thread_utility"
include OrderedHelper

namespace :subscription do
  desc "Job - send email subscription"
  
  task :send_email => :environment do
    puts ">>> Rake subscription:send_email"
    SubscriptionHelper::send_subscription_mail()
    puts ">>> Fim Rake subscription:send_email"
  end

end
