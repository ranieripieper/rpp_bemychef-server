# encoding: UTF-8

#require File.join(File.dirname(__FILE__), '../chopps/cielo/transaction')

require 'rubygems'
require 'net/http'
require 'builder'

#require File.join(File.dirname(__FILE__), '/generate_capa')
desc "Executa todas as tasks"

task :teste_task => :environment do

time = Time.now

puts "------------------------"

transaction = Cielo::Transaction.new

campo_livre = "teste campo livre"
transaction_parameters = {
  numero: "XXX",
  valor: "2000",
  moeda: "986",
  bandeira: 'visa',
  :'campo-livre' => campo_livre,
  autorizar: '3',
  capturar: "false",
  :'url-retorno' => 'http://doisdoissete.com',
  cartao_numero: '4012001037141112',
  cartao_validade: '201805',
  cartao_seguranca: '123',
  cartao_portador: 'Lorem Ipsum Dolor'
}

res = transaction.create!(transaction_parameters, :store) #inicia uma nova transação
#tid = 10069930692541461001

#res = transaction.verify!("10069930692554F61001", "1006993069", "25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3")
puts "xxx"
#res = transaction.catch!("10069930692541971001")
puts "**** #{res}"

if res[:erro].nil? then
  transacao = res[:transacao]
  if not transacao.nil? then
    transaction_id = transacao[:tid]
    status = transacao[:status]
    
    puts "#{transaction_id} = #{status}"
  end
  
else
  puts "#{res[:erro][:codigo]} - #{res[:erro][:mensagem]}"
end



end