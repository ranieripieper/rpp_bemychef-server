# encoding: UTF-8

#require File.join(File.dirname(__FILE__), '../chopps/cielo/transaction')

require 'rubygems'
require 'net/http'
require 'builder'

#require File.join(File.dirname(__FILE__), '/generate_capa')
desc "Executa todas as tasks"

task :populate_base => :environment do

  #save_chefs()
  #uploads_photos()

  address = Address.new
  address.street = "Pardee Homes"
  address.zip_code = 89142
  address.number = 100
  address.city = "Las Vegas"
  address.country = "United States"
  address.state = "Nevada"
  address.latitude = 19
  address.longitude = 20
  address.save
end

def uploads_photos
  16.times do |i|
    if i == 0
      next
    end
    get_photo("/Users/ranieripieper/dev/workspace/227/projects/BeMyChef/imgs_bmc/chef#{i}.png")
    get_photo("/Users/ranieripieper/dev/workspace/227/projects/BeMyChef/imgs_bmc/cozinha#{i}.png")
    get_photo("/Users/ranieripieper/dev/workspace/227/projects/BeMyChef/imgs_bmc/prato#{i}.png")
  end
  
end

def save_chefs
  16.times do |i|
    if i == 0
      next
    end
    user = User.new
    user.name = "Lucas Barboza"
    user.password = "secret123"
    user.password_confirmation = "secret123"
    user.locale = "en"
    user.email = "chef#{i}@bemychef.co"
    
    
    if user.save then
      chef = Chef.new
      chef.about = "todo"
      chef.avg_rating = 1
      chef.nr_rating = 100
      chef.user_id = user.id
      chef.save
    end
  end
end

def get_photo(file_path)
  file = File.open(file_path)
  photo = Photo.new
  photo.file = file
  file.close
  photo.save
  return photo
end

