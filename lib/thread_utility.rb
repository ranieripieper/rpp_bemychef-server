class ThreadUtility
  def self.with_connection(&block)
    begin
      yield block
    rescue Exception => e
      raise e
    ensure
      # Check the connection back in to the connection pool
      ActiveRecord::Base.connection.close if ActiveRecord::Base.connection
    end
  end
end