rm Gemfile.lock
echo ">>>>>> Run bundle install"
bundle install
echo ">>>>>> Run git add"
git add --all
echo ">>>>>> Run commit"
git commit -m "$1"
echo ">>>>>> Run push"
git push $2
