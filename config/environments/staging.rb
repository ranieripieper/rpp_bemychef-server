Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true
  # NOTE: ENV vars aren't available during slug comiplation, so must check if they exist:
  if ENV["REDISCLOUD_URL"]
    config.cache_store = :redis_store, ENV["REDISCLOUD_URL"], { expires_in: 90.minutes }
  end

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like nginx, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable Rails's static asset server (Apache or nginx will already do this).
  config.serve_static_files = false

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true

  config.assets.compress = true
  
  # Generate digests for assets URLs.
  config.assets.digest = true

  # `config.assets.precompile` has moved to config/initializers/assets.rb

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Set to :debug to see everything in the log.
  config.log_level = :info

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = "http://assets.example.com"

  # Precompile additional assets.
  # application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
  # config.assets.precompile += %w( search.js )

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Disable automatic flushing of the log to improve performance.
  # config.autoflush_log = false

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
  
  config.paperclip_defaults = {
    :storage => :s3,
    :s3_credentials => {
      :s3_host_name => ENV['S3_BUCKET_HOST_NAME'],
      :bucket => ENV['S3_BUCKET_NAME'], #testes-dds
      :access_key_id => ENV['AWS_ACCESS_KEY_ID'], #AKIAIDTKLIO5HOIUWXNQ
      :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'] #mqr/wNSd6942PnzK0/Bq35cxlpObNiHTIDCGc2+P
    }
    
    # $ heroku config:set S3_BUCKET_NAME=testes-dds  --app bemychefstg
    # $ heroku config:set AWS_ACCESS_KEY_ID=AKIAIDTKLIO5HOIUWXNQ  --app bemychefstg
    # $ heroku config:set AWS_SECRET_ACCESS_KEY=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  --app bemychefstg
    # $ heroku config:set S3_BUCKET_HOST_NAME=s3-sa-east-1.amazonaws.com  --app bemychefstg
  }
  

  Cielo.setup do |config|
    config.environment = :test #:production
    config.numero_afiliacao = "1006993069" # fornecido pela cielo
    config.chave_acesso = "25fbb99741c739dd84d7b06ec78c9bac718838630f30b112d033ce2e621b34f3" # fornecido pela cielo
    config.return_path = "http://doisdoissete.com" # URL para onde a cielo redirecionara seu usuário após inserir os dados na cielo.
  end
  
  config.action_mailer.default_url_options = { :host => "http://bemychefstg.herokuapp.com/" }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.perform_deliveries = false
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default_options = {from: 'BeMyChef <no-reply@bemychef.co>'}
  config.action_mailer.default :charset => "utf-8"

  config.action_mailer.smtp_settings = {
    address: "smtp.gmail.com",
    port: 587,
    #domain: ENV["GMAIL_DOMAIN"],
    authentication: "plain",
    enable_starttls_auto: true,
    user_name: "smtp227@gmail.com",
    password: "smtpddd@2014"
  }
  
  BeMyChef::Application.config.middleware.use ExceptionNotification::Rack,
    :email => {
      :email_prefix => "[BeMyChef - ERRO] ",
      :sender_address => %{"BeMyChef" <notifier@bemychef.co>},
      :exception_recipients => %w{rani.pieper@doisdoissete.com}
    }
  
end
