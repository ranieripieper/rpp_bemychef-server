require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module BeMyChef
  
  class Application < Rails::Application
    
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    #config.time_zone = "UTC"
    config.active_record.default_timezone = 'Brasilia'
    config.i18n.default_locale = "en".to_sym
    
    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = "pt-BR"
    config.i18n.fallbacks = true
    config.i18n.enforce_available_locales = false
    config.i18n.fallbacks = [:en]
    config.i18n.fallbacks = {'pt' => 'pt-BR'}
    config.assets.version = '1.0'
    #config.autoload_paths += %W(#{config.root}/app/services/be_my_chef)
    config.middleware.use Rack::MethodOverride
    
    config.middleware.delete "ActiveSupport::Cache::Strategy::LocalCache"
    #config.middleware.delete "ActionDispatch::DebugExceptions"
    
    config.active_record.raise_in_transactional_callbacks = true
    
    #config.middleware.insert_before ActionDispatch::ParamsParser, "CatchJsonParseErrors"
  end
end
