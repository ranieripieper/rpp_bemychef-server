Rails.application.routes.draw do


  #root :to => 'application#landing'
  #post  'subscription.html' => 'application#subscription', as: 'chef_subscription'

  get  'resume' => 'application#resume'

  # config/routes.rb
  scope "(:locale)", locale: /en|pt-BR/ do
    root :to => 'application#landing'
    post  'subscription.html' => 'application#subscription', as: 'chef_subscription'
    patch  'subscription.html' => 'application#subscription', as: 'chef_patch_subscription'
    get  'subscription.html' => 'application#landing', as: 'chef_subscription_landing'
  end

  #if not Rails.env.production?
    apipie 
  #end

  devise_for :users, path: "api/v1/users", 
    controllers: { sessions: "api/v1/users/sessions", registrations: 'api/v1/users/registrations', 
                    confirmations: 'api/v1/users/confirmations', passwords: 'api/v1/users/passwords'},
    path_names: { sign_in: 'login', sign_out: 'logout', confirmation: 'confirmation' }

  devise_scope :user do
    get   'user/:user_id/confirmed.html'                          => 'api/v1/users/confirmations#confirmed', as: 'user_confirmed'
    post  'api/v1/users/reconfirm'                                => 'api/v1/users/confirmations#reconfirm'
    post  'api/v1/users/recoverable'                              => 'api/v1/users/registrations#recoverable', as: 'user_recoverable'
    
    post  'api/v1/users/push/save_token'                          => 'api/v1/users/registrations#save_push_token', as: 'user_save_push_token'
  end

  namespace :site do
     get  'index.html'                    => 'store#index'
  end
    
  namespace :api do
    namespace :v1 do   
      #notifications
      get  'notifications/chef.json'                    => 'notifications#get_chef_notifications'
      get  'notifications/user.json'                    => 'notifications#get_user_notifications'
      get  'notifications/chef/:notification_id.json'   => 'notifications#get_chef_notification_by_id'
      get  'notifications/user/:notification_id.json'   => 'notifications#get_user_notification_by_id'

      #chefs
      get  'chef/info/:chef_id'                         => 'chefs#get_chef_by_id'
      get  'user/chef/:user_id'                         => 'chefs#get_chef_by_user_id'
      get  'chef/payment/resume'                        => 'chefs#get_payment_resume'
      
      #ingredientes
      get  'ingredients/get_all'                        => 'ingredients#get_all'  
      get  'ingredients/get_dish_type'                  => 'ingredients#get_dish_type'
      
      #kitchens     
      post  'chef/kitchens/create'                              => 'kitchens#create'
      get  'chef/kitchens'                                      => 'kitchens#get_kitchens'
      get  'chef/kitchen'                                       => 'kitchens#get_first_kitchen'
      get  'chef/:chef_id/kitchens'                             => 'kitchens#get_kitchens_by_chef'
      get  'user/chef/:user_chef_id/kitchens'                   => 'kitchens#get_kitchens_by_user_chef_id'
      get  'kitchens/:kitchen_id'                               => 'kitchens#get_kitchen_by_id'
      post  'chef/kitchens/:kitchen_id/close'                   => 'kitchens#close_kitchen'
      post  'chef/kitchens/:kitchen_id/open'                    => 'kitchens#open_kitchen'
      put  'chef/kitchens/:kitchen_id'                          => 'kitchens#update_kitchen'
      delete  'chef/kitchens/:kitchen_id/photo/:photo_id'       => 'kitchens#delete_photo'

      #dishes     
      get     'dishes/find_near'                           => 'dishes#find_near'
      post    'dishes/insert'                              => 'dishes#insert_dish'
      delete  'dishes/:dish_id/delete'                     => 'dishes#delete_dish'
      get     'dishes/:dish_id'                            => 'dishes#get_by_dish_id'
      put     'dishes/:dish_id/update'                     => 'dishes#update_dish'
      get     'dishes/ordered/:dish_id'                    => 'dishes#ordered_dish'
      
      #ordereds     
      post  'ordereds/dishes/reserve/:dish_id'                    => 'ordereds#reserve_dish'
      delete  'ordereds/reserve/:reserve_id/cancel'                 => 'ordereds#cancel_reserve'
      #post   'ordereds/reserve/:reserve_id/confirm'               => 'ordereds#confirm_reserve'      
      post   'ordereds/reserve/:reserve_id/pagar.me/confirm'      => 'ordereds#confirm_reserve_pagar_me'      
      get   'ordereds/:ordered_id'                                => 'ordereds#get_ordered_by_id'
      post  'ordereds/:ordered_id/accept'                         => 'ordereds#accept_ordered'
      post  'ordereds/:ordered_id/cancel_by_chef'                 => 'ordereds#cancel_ordered_by_chef'
      post  'ordereds/:ordered_id/ordered_ready'                  => 'ordereds#ordered_ready'
      get   'ordereds.json'                                       => 'ordereds#get_history_ordereds'
      get   'user/ordereds.json'                                  => 'ordereds#get_user_history_ordereds'
      
      #addresses
      get  'user/addresses'                             => 'addresses#get_addresses_delivered'
      get  'user/addresses/:cep'                        => 'addresses#get_address_by_cep'
      
      #get  'ordereds/:ordered_id/cancel_by_user'        => 'ordereds#cancel_ordered_by_user'
      
      #comments
      post  'comments/:ordered_id/:dish_id/create'                   => 'comments#create'
      put   'comments/:comment_id'                                   => 'comments#update'
      get   'dishes/:dish_id/comments.json'                     => 'comments#get_comments'
      post   'partner/authorize'                     => 'comments#get_comments'
      #get   'comments/:ordered_id/:dish_id/my_comment.json'                   => 'comments#get_comment_by_dish_user'
      
      #users
      get   'users/me.json'                                  => 'users#get_user_info'
          
    end 
  end
end
