Correios::CEP.configure do |config|
  config.log_enabled = false   # It disables the log
  config.logger = Rails.logger # It uses Rails logger
end