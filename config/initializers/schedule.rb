=begin
require 'rufus-scheduler'
require "#{Rails.root}/lib/thread_utility"

s = Rufus::Scheduler.singleton
  
s.every "30m" do
  ThreadUtility.with_connection do
    puts "******Envia email subscription **********"  
    SubscriptionHelper::send_subscription_mail()
    puts "******Fim Envia email subscription **********"
  end
end
  

if ENV['RAILS_ENV'] != 'production' then
  if not Rails.env.production? then
    s = Rufus::Scheduler.singleton
    
    
    s.every "#{MainYetting.TIMEOUT_ACCEPT_ORDERED_TASK}s" do
      ThreadUtility.with_connection do
        puts "******Verifica pedidos**********"  
        OrderedHelper::ordered_timeout()
        puts "****************"
      end
    end
    
    s.every "#{MainYetting.TIMEOUT_CONFIRM_RESERVE_TASK}s" do
      ThreadUtility.with_connection do
        puts "******Verifica as reservas**********"  
        OrderedHelper::reserve_timeout()
        puts "****************"
      end
    end
    
    
    s.cron '5 0 * * *' do
      ThreadUtility.with_connection do
        # do something every day, five minutes after midnight
        puts "******Executa cancelamento de PAGAMENTOS**********"  
        PaymentHelper::cancel_payments()
        puts "****************"
      end
    end
  end
end
=end