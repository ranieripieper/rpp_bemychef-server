# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( respond.min.js )
Rails.application.config.assets.precompile += %w( home_select.js )
Rails.application.config.assets.precompile += %w( vendor/jquery.blImageCenter.js )
Rails.application.config.assets.precompile += %w( vendor/bootstrap-select.min.js )
Rails.application.config.assets.precompile += %w( vendor/flat-ui.min.js )
Rails.application.config.assets.precompile += %w( vendor/bootstrap.min.js )

Rails.application.config.assets.precompile += %w( vendor/bootstrap.min.css )
Rails.application.config.assets.precompile += %w( home_select.css )
Rails.application.config.assets.precompile += %w( boilerplate.css )
Rails.application.config.assets.precompile += %w( vendor/flat-ui.css )
Rails.application.config.assets.precompile += %w( vendor/bootstrap-select.min.css )

Rails.application.config.assets.precompile += %w( glyphicons/flat-ui-icons-regular.ttf )
