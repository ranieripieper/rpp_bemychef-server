if ENV['RAILS_ENV'] != 'staging' then
  Geocoder.configure(
    :http_proxy => ENV['QUOTAGUARD_URL'],
    :timeout => 5
  )
end