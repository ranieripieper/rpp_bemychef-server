Apipie.configure do |config|
  config.app_name                = "BeMyChef"
  config.api_base_url            = ""
  config.doc_base_url            = "/api/doc"
  config.validate                = true
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
end
