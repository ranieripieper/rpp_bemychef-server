class IntegerValidator < Apipie::Validator::BaseValidator

  def initialize(param_description, argument)
    super(param_description)
    @type = argument
  end

  def validate(value)
    return false if value.nil?
    !!(value.to_s =~ /^[-+]?[0-9]+$/)
  end

  def self.build(param_description, argument, options, block)
    if argument == Integer || argument == Fixnum
      self.new(param_description, argument)
    end
  end

  def description
    "Must be #{@type}."
  end
end

class FloatValidator < Apipie::Validator::BaseValidator

  def initialize(param_description, argument)
    super(param_description)
    @type = argument
  end

  def validate(value)
    return false if value.nil?
    return true if value =~ /^\d+$/
    true if Float(value.to_s) rescue false
  end

  def self.build(param_description, argument, options, block)
    if argument == Float || argument == Fixnum
      self.new(param_description, argument)
    end
  end

  def description
    "Must be #{@type}."
  end
end