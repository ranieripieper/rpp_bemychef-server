#Exception Handler
###
#You can add different settings using this block
#Use the docs at http://github.com/richpeck/exception_handler for info
###
ExceptionHandler.setup do |config|

	#DB - 
	#Options = false / true
	config.db = false

	#Email -
	#Default = false / true
	#config.email = 

	#Social
	config.social = {
		fb: 	"http://www.facebook.com/bemychef.co",
		twitter:  "https://twitter.com/Be_MyChef",
		instagram:   "http://instagram.com/bemychef_",
		pinterest:   "http://www.pinterest.com/be_mychef/",
		mail:   "mailto:email@bemychef.co"
	}

end